/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.wicketwiz.mavenplugin;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.maven.artifact.DependencyResolutionRequiredException;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.wicket.MarkupContainer;
import org.bitbucket.wicketwiz.core.WicketWizFactory;
import org.bitbucket.wicketwiz.core.introspector.ComponentInjector;
import org.bitbucket.wicketwiz.core.introspector.InjectorError;
import org.bitbucket.wicketwiz.core.introspector.InjectorValidationException;
import org.bitbucket.wicketwiz.core.model.HostCompound;

/**
 *
 * @author count
 */
@Mojo( name = "validate", defaultPhase = LifecyclePhase.PROCESS_CLASSES, requiresDependencyResolution = ResolutionScope.COMPILE )
public class WicketWizValidatorMojo extends WicketWizMojo {
	
	// from: ﻿http://stackoverflow.com/questions/11341783/accessing-classes-in-custom-maven-reporting-plugin
	/**
	 * The classpath elements of the project.
	 */
	@Parameter(property = "project.runtimeClasspathElements", required = true, readonly = true)
	private List<String> classpathElements;

	private ClassLoader getProjectClassLoader() throws DependencyResolutionRequiredException, MalformedURLException
	{
		List<String> classPath = new ArrayList<String>();
		classPath.addAll( classpathElements );
		classPath.add( project.getBuild().getOutputDirectory() );
		URL[] urls = new URL[classPath.size()];
		int i = 0;
		for ( String entry : classPath )
		{
			getLog().debug( "use classPath entry " + entry );
			urls[i] = new File( entry ).toURI().toURL();
			i++; // Important
		}
		return new URLClassLoader( urls, getClass().getClassLoader() );
	}


	private ComponentInjector injector;

	
	@Override
	protected boolean hostCompoundParsed(HostCompound hostCompound) throws MalformedURLException  {
		String className = hostCompound.getTargetClassName().getName();
		Class<? extends MarkupContainer> containerClass;

		Class clazz;
		try {
			clazz = getProjectClassLoader().loadClass(className);
		} catch(ClassNotFoundException cnfx) {
			getLog().error(String.format("could not find class '%s', but a corresponding XHTML file with WicketWiz namespace exists", className));
			return false;
		} catch (DependencyResolutionRequiredException ex) {
			getLog().error(ex);
			return false;
		}
		try {
			containerClass = clazz.asSubclass(MarkupContainer.class);
		} catch(ClassCastException ccx) {
			getLog().error(String.format("The class '%s' needs to be a subclass of '%s'", clazz.getName(), MarkupContainer.class.getName()));
			return false;
		}

		try {
			injector.validate(containerClass, hostCompound);
		} catch (InjectorValidationException ex) {
			getLog().error(String.format("Errors found while validating class %s", containerClass.getName()));
			for(InjectorError error : ex.getErrors()) {
				getLog().error("\t" + error.formatMessage());
			}
			return false;
		}
		
		return true;
	}

	public void execute() throws MojoExecutionException, MojoFailureException {
		WicketWizFactory factory = WicketWizFactory.newInstance();
		injector = factory.createInjector();
		
		executeImpl();
	}
	
	
	
}
