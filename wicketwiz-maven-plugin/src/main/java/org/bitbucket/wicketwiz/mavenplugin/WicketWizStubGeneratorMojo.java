/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.wicketwiz.mavenplugin;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.bitbucket.wicketwiz.core.WicketWizFactory;
import org.bitbucket.wicketwiz.core.codegen.CodeGenerator;
import org.bitbucket.wicketwiz.core.model.HostCompound;

/**
 *
 * @author count
 */
@Mojo( name = "stubs", defaultPhase = LifecyclePhase.GENERATE_SOURCES, requiresDependencyResolution = ResolutionScope.COMPILE )
public class WicketWizStubGeneratorMojo extends WicketWizMojo{
	
    /**
     * Location of the file.
     */
    @Parameter( defaultValue = "${project.build.directory}/generated-sources/wicketwiz/", property = "outputDir", required = true )
    private File outputDirectory;
	private CodeGenerator codegen;
	
	@Override
	protected boolean hostCompoundParsed(HostCompound hostCompound) throws IOException {
		getLog().debug(String.format("generating code for target class %s", hostCompound.getTargetClassName().getName()));

		CodeGenerator codegen = WicketWizFactory.newInstance().createCodeGenerator(toDirUri(outputDirectory));
		// FIXME: the code generator SHOULD do more validation. In turn, 
		// it will then through exceptions that need further processing.
		codegen.generate(hostCompound);
		return true;
	}

	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
        if ( !outputDirectory.exists() )
        {
            outputDirectory.mkdirs();
        }
		
		WicketWizFactory factory = WicketWizFactory.newInstance();
		

		codegen = factory.createCodeGenerator(toDirUri(outputDirectory));
		
		executeImpl();
		
	}
	
	
}
