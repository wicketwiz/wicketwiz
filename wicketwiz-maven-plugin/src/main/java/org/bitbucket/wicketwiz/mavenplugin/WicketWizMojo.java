package org.bitbucket.wicketwiz.mavenplugin;

/*
 * Copyright 2001-2005 The Apache Software Foundation.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.apache.maven.model.Resource;

import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;

import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;
import org.apache.maven.shared.model.fileset.FileSet;
import org.apache.maven.shared.model.fileset.util.FileSetManager;
import org.bitbucket.wicketwiz.core.WicketWizFactory;
import org.bitbucket.wicketwiz.core.codegen.CodeGenerator;
import org.bitbucket.wicketwiz.core.model.HostCompound;
import org.bitbucket.wicketwiz.core.parser.Location;
import org.bitbucket.wicketwiz.core.parser.PageParser;
import org.bitbucket.wicketwiz.core.parser.PageParserException;
import org.bitbucket.wicketwiz.core.parser.ParserError;

/**
 * Goal which touches a timestamp file.
 *
 */
public abstract class WicketWizMojo
    extends AbstractMojo
{

	@Parameter( defaultValue = "${basedir}", property = "basedir", required = true)
	private File basedir;
	
	@Parameter( defaultValue = ".html .xhtml .xml", property = "suffixes", required = true)
	private String suffixes;
	
	@Parameter(property = "project", required = true, readonly = true)
    protected MavenProject project;	
	
    /**
 	 * The list of resources we want to transfer.
 	 */
 	@Parameter( defaultValue = "${project.resources}", required = true, readonly = true )
 	private List<Resource> resources;

	protected void executeImpl()
        throws MojoExecutionException, MojoFailureException
    {
		String[] suffixArray = suffixes.split("\\s+");
		
		WicketWizFactory factory = WicketWizFactory.newInstance();
		
		FileSetManager fsm = new FileSetManager(getLog());
		
		boolean success = true;
		
		for(Resource resource : resources) {
			
			getLog().info("scanning resources in: " + resource.getDirectory());
			int numFilesProcessed = 0;
			
			File resourceDirFile = new File(resource.getDirectory());
			URI basedirUri = toDirUri(basedir);
			URI baseUri = toDirUri(basedirUri.resolve(resourceDirFile.toURI()));
			
			getLog().debug("base dir: " + baseUri);
			PageParser pageParser = factory.createPageParser(baseUri);
			
			FileSet fs = new FileSet();
			fs.setDirectory(resource.getDirectory());
			fs.setExcludes(resource.getExcludes());
			fs.setIncludes(resource.getIncludes());
			
			for(String path : fsm.getIncludedFiles(fs)) {
				boolean suffixMatch = false;
				
				getLog().debug(suffixArray.toString());
				for(String suffix : suffixArray) {
					if(path.endsWith(suffix)) {
						suffixMatch = true;
						
					}
				}
				
				getLog().debug("path: " + path);
				if(!suffixMatch) {
					continue;
				}
				
				URI pathUri = URI.create(path.replace(File.separatorChar, '/'));
				HostCompound hostCompound = null;
				
				boolean parsedSuccessfully = false;
				try {
					hostCompound = pageParser.parsePage(pathUri);
					parsedSuccessfully = true;
				} catch (IOException ex) {
					getLog().error(String.format("cannot read from file while parsing %s", uriToPathString(pathUri)), ex);
				} catch (PageParserException ex) {
					getLog().error(String.format("errors occurred while parsing file %s", uriToPathString(pathUri)));
					for(ParserError e : ex.getErrors()) {
						String locationString = locationToString(e.getLocation());
						
						getLog().error(String.format("%s:", locationString));
						getLog().error(String.format("\t%s", e.formatMessage()));
					}
				}
				
				success = success && parsedSuccessfully;
				if(!parsedSuccessfully) {
					continue;
				}
				
				if(hostCompound != null) {
					boolean processedSuccessfully = false;
					
					try {
						processedSuccessfully = hostCompoundParsed(hostCompound);
					} catch(IOException ex) {
						getLog().error(ex);
					}
					
					success = success && processedSuccessfully;
						
					++numFilesProcessed;
				} else {
					getLog().debug(String.format("no WicketWiz attributes found in page %s, ignoring file.", pathUri.toString()));
				}
				
			}
			
			getLog().info(String.format("processed %d files.", numFilesProcessed));
			
		}
		
		if(!success) {
			throw new MojoFailureException("Processing failed for one or more files.");
		}
    }

	protected abstract boolean hostCompoundParsed(HostCompound hostCompound) throws IOException;
	
	protected URI toDirUri(File dir) {
		URI uri = dir.toURI();
		return toDirUri(uri);
	}
	
	protected URI toDirUri(URI uri) {
		String uriString = uri.toASCIIString();
		if(!uriString.endsWith("/")) {
			return URI.create(uriString + "/");
		} else {
			return URI.create(uriString);
		}
	}

	private String locationToString(Location location) {
		if(location == null) {
			return "<unknown location>";
		}
		StringBuilder sb = new StringBuilder();
		sb.append(uriToPathString(location.getUri()));
		sb.append('(');
		sb.append(location.getLine());
		sb.append(',');
		sb.append(location.getColumn());
		sb.append(')');
		return sb.toString();
	}
	
	private String uriToPathString(URI locationUri) {
		if (locationUri == null) {
			return "<unknown file>";
		} else {
			return "file".equals(locationUri.getScheme()) ? locationUri.getPath() : locationUri.toString();
		}
	}
}
