# WicketWiz #

WicketWiz is a toolkit to make [Apache Wicket](http://wicket.apache.org) development easier. An IOC runtime provides Wicket component injection capabilities similar to those known from the Spring framework, and a Maven plugin validates your WicketWiz-enabled UIs at build time, catching those bugs early.

So in essence, your Java UI code transforms from...

    class FooPanel extends Panel {
        private Form form;
        private TextField nameTextField;
        private TextField addressTextField;
        private AjaxButton addButton;

        FooPanel(String id) {
            super(id);

            // component construction boilerplate code
            form = new Form("form");
            add(form);

            nameTextField = new TextField("nameTextField");
            form.add(nameTextField);
            addressTextField = new TextField("addressTextField");
            form.add(addressTextField);
            submitButton = new AjaxButton("submitButton");
            form.add(submitButton);
            
            // add behaviours, etc.
            ...
        }

...to this:

    class FooPanel extends Panel {
        @Wiz
        private Form form;
        @Wiz
        private TextField nameTextField;
        @Wiz
        private TextField addressTextField;
        @Wiz
        private AjaxButton addButton;

        FooPanel(String id) {
            super(id);

            // look, no boilerplate!!
            
            // add behaviours, etc.
            ...
        }

All you need in XHTML is to bind the WicketWiz namespace with `xmlns:wiz="urn:org.bitbucket.wicketwiz.markup.ns"`. So your XHTML files should start like this:

	<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
	<html 
		xmlns="http://www.w3.org/1999/xhtml"
		xmlns:wicket="http://wicket.apache.org" 
		xmlns:wiz="urn:org.bitbucket.wicketwiz.markup.ns">
		...

The namespace binding will signal the component injector that injection is enabled for this file, which in turn will scan the associated Java class for injection points (fields annotated with `@Wiz`). The same goes for the validator plugin; it will validate only classes where injection is enabled. Without the namespace binding, `@Wiz` annotations are ignored.


## Quick Start ##

This will get you going ASAP with WicketWiz. For more information, have a look at the [Motivation section][motivation].

### Basic Setup ###
First you need to add the WicketWiz runtime to your `pom.xml`:

	<repositories>
		...
		<repository>
			<id>sonatype-oss</id>
			<url>https://oss.sonatype.org/content/groups/public</url>
			<snapshots>
				<enabled>true</enabled>
			</snapshots>
		</repository>
		...
	</repositories>
	...
	
	<dependencies>
		...
		<dependency>
			<groupId>org.bitbucket.wicketwiz</groupId>
			<artifactId>wicketwiz-core</artifactId>
			<version>0.1-SNAPSHOT</version>
		</dependency>
		<dependency>
			<groupId>org.bitbucket.wicketwiz</groupId>
			<artifactId>wicketwiz-core-impl</artifactId>
			<version>0.1-SNAPSHOT</version>
		</dependency>
		...

Also add the validator plugin, like this:

	<build>
		<plugins>
			...

			<plugin>
				<groupId>org.bitbucket.wicketwiz</groupId>
				<version>0.1</version>
				<artifactId>wicketwiz-maven-plugin</artifactId>
				<executions>
					<execution>
						<goals>
							<goal>validate</goal>
						</goals>
					</execution>
				</executions>
			</plugin>

			...
		</plugins>
		...
	</build>

In your Wicket application class, you'll need to add the WicketWiz injector. For Wicket 1.4, this looks like this:

	public class MyWicketApplication extends WebApplication{
	
		public void init() {
			
			super.init();
			
			// Add WicketWiz injector as component instantiation listener, so that when a component is created, 
			// its subcomponents are injected if the assocated XHTML markup binds the WicketWiz XML namespace.
			addComponentInstantiationListener(new WicketWizComponentInjector(this, true));
		}
	}

In Wicket 1.5, the call to add instantiation listeners changed, so your code will look something like this:

	public class MyWicketApplication extends WebApplication{
		public void init() {
			super.init();
			getComponentInstantiationListeners().add(new WicketWizComponentInjector(this));
		}
	}


Now that you've set everything up, build and run your application. It should startup and function normally, like before. The only difference you'll notice is that, at build time, you'll see some output from the validator plugin, like this:

	--- wicketwiz-maven-plugin:0.1:validate (default) @ my-ui-project ---
	scanning resources in: /Users/upachler/Documents/my-ui-project/src/main/resources
	processed 11 files.

The reason that you shouldn't see anything more is because WicketWiz is an opt-in mechanism, which is enabled on a per HTML file basis. You can now add the WicketWiz to your XHTML files one by one, allowing for an easy transition.

## Motivation ##
[motivation]:
[Apache Wicket](http://wicket.apache.org) is a widely used Web UI framework. At its core, a single Wicket UI page consists of a XHTML file and a Java class backing that page. So for a HomePage.html, there needs to be a HomePage.java, etc. The HTML elements representing Wicket Components in HomePage.java need to carry a `wicket:id='some-id'` attribute. At runtime, Wicket then matches the Wicket IDs in XHTML and Java code to bind components to markup. Since components can be nested in XHTML, they also need to be nested the same way in Java code by calling `parentComponent.add(childComponent)`.

So in essence, in a Wicket UI page, the following invariants need to hold:

* For each Wicket ID in your XHMLT, there needs to be a Wicket Component with the same ID in the corresponding Java class
* The nesting hierarchies in your Java Code need to match the structure of your XHTML

Because Wicket only checks these invariants at runtime, development can take quite a while: Errors are only found when running your application and navigating to the respective UI, so that it gets rendered in your browser. And then, only one error is found at a time. Also, the code required to nest components in Java seems superfluous, because the hierarchy is already defined in your XHTML. In essence, duplicating the structure in Java is unnecessary.

WicketWiz aims to address these issues in two ways:

* Components are no longer created and nested in your code. Instead, they're injected by the WicketWiz runtime library. Your code will see a properly created and nested Wicket component hierarchy when your UI is created, without any boilerplate code.
* Injection requirements (such as Wicket ID matching) are checked at build time by a Maven plugin