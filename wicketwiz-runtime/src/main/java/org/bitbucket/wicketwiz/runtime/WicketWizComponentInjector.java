/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.wicketwiz.runtime;

import org.bitbucket.wicketwiz.runtime.glue.WicketGlue;
import java.io.IOException;
import java.net.URI;
import org.apache.wicket.Component;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.application.IComponentInstantiationListener;
import org.apache.wicket.protocol.http.WebApplication;
import org.bitbucket.wicketwiz.core.WicketWizFactory;
import org.bitbucket.wicketwiz.core.introspector.ComponentInjector;
import org.bitbucket.wicketwiz.core.introspector.InjectorValidationException;
import org.bitbucket.wicketwiz.core.model.HostCompound;
import org.bitbucket.wicketwiz.core.parser.PageParser;
import org.bitbucket.wicketwiz.core.parser.PageParserException;


/**
 * Enables WicketWiz component injection for WebMarkupContainer-derived classes
 * which have @Wiz-defined injection targets and the WicketWiz namespace 
 * mapped in their XHTML.<p>
 * 
 * To use, you need to add an instance of this class as component instantiation
 * listener to your Wicket application object.
 * 
 * <pre> 
 public class MyWicketApplication extends WebApplication{
    &#40;Override
	public void init() {
        super.init();

        // ... your other init code

        // add an instance of this class as instantiation listener
        addComponentInstantiationListener(new WicketWizComponentInjector(this));
    }
}
</pre>
 */
public class WicketWizComponentInjector implements IComponentInstantiationListener{
	private final WebApplication application;
	private WicketWizFactory factory;
	private WicketGlue wicketGlue;
	private boolean ignoreParserErrors;
	private final ComponentInjector injector;
	
	public WicketWizComponentInjector(WebApplication application) {
		this(application, false);
	}
	public WicketWizComponentInjector(WebApplication application, boolean ignoreParserErrors) {
		this.application = application;
		
		factory = WicketWizFactory.newInstance();
		injector = factory.createInjector();
		
		try {
			wicketGlue = WicketGlue.newWicketGlue(application);
		} catch (ClassNotFoundException ex) {
			throw new RuntimeException("Could not find glue class, make sure you linked the appropriate wicketwiz glue library for you wicket version", ex);
		}
		
		this.ignoreParserErrors = ignoreParserErrors;
	}

	public void onInstantiation(Component component) {
		if(!(component instanceof MarkupContainer)) {
			return;
		}
		MarkupContainer mc = (MarkupContainer)component;
		
		HostCompound markupModel = null;

		URI markupURI = wicketGlue.findMarkupURI(mc);
		if(markupURI != null) {
			String packagePath = component.getClass().getPackage().getName().replace('.', '/') + '/';
			URI markupBaseUri = getMarkupBaseUri(markupURI, packagePath);
			PageParser pp = factory.createPageParser(markupBaseUri);
			RuntimeException rx = null;
			try {
				markupModel = pp.parsePage(markupURI);
			} catch (IOException ex) {
				rx = new RuntimeException(ex);
			} catch (PageParserException ex) {
				rx = new RuntimeException(ex);
			}

			if(rx!=null && !ignoreParserErrors) {
				throw rx;
			}
		}
		// if markupModel == null at this point, 
		// we could not locate XHTML markup or it has no WicketWiz information
		// (that is, no xmlns:wiz=.. XML namespace definition before the
		// first Wicket tag)
		// We'll still attempt injection, because the component
		// instantiation may have installed a context on the component
		// injector. This will be the case for components like ListItems or
		// fragments
		
		try {
			injector.injectComponents(mc, markupModel);
		} catch (InjectorValidationException ex) {
			throw new RuntimeException(ex);
		}
	}
	
	private URI getMarkupBaseUri(URI markupURI, String packagePath) {
		String uriString;
		if("jar".equals(markupURI.getScheme())) {
			uriString = markupURI.toString();
			int slashIndex = uriString.lastIndexOf('/');
			uriString = uriString.substring(0, slashIndex+1);
		} else {
			URI basePathUri = markupURI.resolve(".");
			uriString = basePathUri.toString();
		}
		
		// FIXME: in case we ever get query strings here as input, this will
		// fall over..
		if(!uriString.endsWith(packagePath)) {
			throw new IllegalArgumentException("component markup URI and corresponding component package do not match");
		}
		
		uriString = uriString.substring(0, uriString.length()-packagePath.length());
		return URI.create(uriString);
	}
	
}
