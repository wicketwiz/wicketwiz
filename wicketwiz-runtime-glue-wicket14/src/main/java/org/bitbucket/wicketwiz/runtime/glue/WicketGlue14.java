/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.wicketwiz.runtime.glue;

import org.bitbucket.wicketwiz.runtime.glue.WicketGlue;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.markup.MarkupStream;
import org.apache.wicket.util.resource.FileResourceStream;
import org.apache.wicket.util.resource.IResourceStream;
import org.apache.wicket.util.resource.UrlResourceStream;

/**
 *
 * @author upachler
 */
public class WicketGlue14 extends WicketGlue {

	@Override
	public URI findMarkupURI(MarkupContainer mc) {
		MarkupStream ms = mc.getMarkupStream();
		try {
			if(ms == null) {
				Class<? extends MarkupContainer> clazz = mc.getClass();
				URL url = clazz.getResource("/" + clazz.getName().replace('.', '/') + ".html");
				if(url != null) {
					return url.toURI();
				} else {
					return null;	// no markup
				}
			}
			IResourceStream rs = ms.getResource();
			if(rs instanceof UrlResourceStream) {
				return ((UrlResourceStream)rs).getURL().toURI();
			} else if(rs instanceof FileResourceStream) {
				return ((FileResourceStream)rs).getFile().toURI();
			} else {
				throw new RuntimeException(String.format("cannot get markup URI from resource class '%s'", rs.getClass().getName()));
			}
		} catch (URISyntaxException ex) {
			throw new RuntimeException(ex);
		}
	}
	
}
