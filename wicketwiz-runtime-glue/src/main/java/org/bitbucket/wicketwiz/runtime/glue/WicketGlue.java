/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.wicketwiz.runtime.glue;

import java.net.URI;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.protocol.http.WebApplication;

/**
 *
 * @author upachler
 */
public abstract class WicketGlue {
	private static final String WICKET_GLUE_1_4_CLASSNAME = WicketGlue.class.getPackage().getName() + ".WicketGlue14";
	private static final String WICKET_GLUE_1_5_PLUS_CLASSNAME = WicketGlue.class.getPackage().getName() + ".WicketGlue15";
	
	private static Pattern WICKET_VERSION_PATTERN = Pattern.compile("^(\\d+)\\.(\\d+)");
	
	public static WicketGlue newWicketGlue(WebApplication wicketApp) throws ClassNotFoundException {
		String wicketVersionString = wicketApp.getFrameworkSettings().getVersion();
		Matcher m = WICKET_VERSION_PATTERN.matcher(wicketVersionString);
		if(!m.find()) {
			throw new RuntimeException(String.format("cannot parse Wicket version '%s'", wicketVersionString));
		}
		
		int major = Integer.parseInt(m.group(1));
		int minor = Integer.parseInt(m.group(2));
		
		String className;
		if(major == 1 && minor < 5) {
			className = WICKET_GLUE_1_4_CLASSNAME;
		} else {
			className = WICKET_GLUE_1_5_PLUS_CLASSNAME;
		}
		
		Class clazz = Class.forName(className);
		try {
			return (WicketGlue)clazz.newInstance();
		} catch (IllegalAccessException ex) {
			throw new RuntimeException(ex);
		} catch (InstantiationException ex) {
			throw new RuntimeException(ex);
		}
	}

	public abstract URI findMarkupURI(MarkupContainer mc);
}
