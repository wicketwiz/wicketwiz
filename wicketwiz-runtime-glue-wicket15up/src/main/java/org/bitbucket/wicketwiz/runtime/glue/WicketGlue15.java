/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.wicketwiz.runtime.glue;

import java.net.URI;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.markup.Markup;
import org.apache.wicket.markup.MarkupFactory;
import org.bitbucket.wicketwiz.runtime.glue.WicketGlue;

/**
 *
 * @author upachler
 */
public class WicketGlue15 extends WicketGlue {

	@Override
	public URI findMarkupURI(MarkupContainer mc) {
		Markup markup = MarkupFactory.get().getMarkup(mc, false);
		
		if(markup == null) {
			return null;	// no markup for this class
		}
		String markupLocation = markup.locationAsString();
		URI markupURI = URI.create(markupLocation);
		return markupURI;
	}
	
}
