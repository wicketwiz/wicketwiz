/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.wicketwiz.core.parser;

/**
 *
 * @author count
 */
public interface ParserError {
	
	public enum Type {
		/**
		 * The XHTML file cannot be parsed by the underlying XML parser. It
		 * appers that the file is not XML compliant and contains
		 * syntax errors.
		 *//**
		 * The XHTML file cannot be parsed by the underlying XML parser. It
		 * appers that the file is not XML compliant and contains
		 * syntax errors.
		 */
		MALFORMED_XML,
		/**
		 * The 'xmlns:wiz=..' namespace declaration must appear before 
		 * the first wicket:id=.. attribute, or not at all.
		 */
		WIZ_NS_AFTER_FIRST_WICKETID,
		/**
		 * The 'xmlns:wiz=..' namespace declaration may only occur once
		 * in a file, but it occurs multiple times
		 */
		WIZ_NS_OCCURS_MULTIPLE_TIMES,
		/**
		 * The wiz:ignore=.. attribute contains an invalid value, allowed
		 * values are only 'true' or 'false'
		 */
		WIZ_IGNORE_ATTRIBUTE_MALFORMED,
		/**
		 * On Elements where wiz:baseclass is defined, the attributes
		 * wiz:componentclass and wicket:id may not occur.
		 */
		BASECLASS_ATTRIBUTE_CONFLICT,
		/**
		 * The baseclass was already defined previously in this file, but
		 * may only be defined once.
		 */
		BASECLASS_ATTRIBUTE_ALREADY_DEFINED,
		/**
		 * The componentclass attribute may only occur on elements that
		 * also have wicket:id defined on them.
		 */
		COMPONENTCLASS_ATTRIBUTE_WITHOUT_WICKETID,
		/**
		 * The name specified is not a valid Java class name
		 */
		CLASSNAME_MALFORMED,
	}
	
	Location getLocation();
	
	Type getType();
	
	String formatMessage();
	
}
