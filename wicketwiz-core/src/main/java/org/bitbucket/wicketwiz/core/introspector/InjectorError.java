/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.wicketwiz.core.introspector;

import java.lang.reflect.Constructor;

/**
 *
 * @author count
 */
public interface InjectorError {
	enum Type {
		/**
		 * There is no injection point available matching the declared
		 * wicket ID.
		 */
		MISSING_INJECTION_POINT,
		/**
		 * There is an injection point, but no markup exists that carries
		 * the injection point's property name as Wicket ID.
		 */
		MISSING_COMPONENT_INSTANCE,
		/**
		 * The injection point matches markup that is ignored by the injector.
		 * The markup is ignored because one of its ancestors is a repeater
		 * or other obscuring component type.
		 */
		INJECTION_POINT_MAPPED_TO_IGNORED_COMPONENT_INSTANCE,
		/**
		 * The class declared for the component cannot be found (this will
		 * typically occur if a component class was specified in the markup).
		 */
		COMPONENT_CLASS_NOT_FOUND,
		/**
		 * The class declared for the component is not a subclass of 
		 * Wicket's Component class.
		 */
		COMPONENT_INSTANCE_CLASS_NOT_COMPONENT,
		/**
		 * The component cannot be injected into the target field because 
		 * the component's type cannot be converted to the field's type.
		 */
		INCOMPATIBLE_FIELD_TYPE,
		/**
		 * Occurs when the component class to instantiate is an inner class, 
		 * and the enclosing class is not the same class as the container's
		 * class. WicketWiz allows inner classes to be used as component classes,
		 * but in order to instantiate them, it needs to provide the enclosing
		 * instance as a constructor argument - and all it has for that is
		 * the container.
		 */
		ENCLOSING_CLASS_NOT_CONTAINER_CLASS,
		/**
		 * The component cannot be instantiated because WicketWiz cannot find
		 * a suitable constructor on the component class. For regular component
		 * classes, a single argument constructor taking the wicket ID as 
		 * String is required. Note that nonstatic inner class constructors
		 * take two arguments, where the first argument is hidden
		 */
		COMPONENT_CONSTRUCTOR_NOT_FOUND,
		/**
		 * The component's constructor is not accessible and/or cannot be
		 * made accessible by calling the {@link Constructor#setAccessible(boolean) }
		 * method. This points to a Java SecurityManager configuration problem.
		 */
		CONSTRUCTOR_INACCESSIBLE,
		/**
		 * The specified factory method was not found. Note the method name must
		 * match, and the method needs to take a single String argument.
		 */
		FACTORY_METHOD_NOT_FOUND,
		/**
		 * The specified component factory method's return type does not
		 * is not assignment compatible with the injection point's type.
		 */
		FACTORY_METHOD_RETURN_TYPE_INCOMPATIBLE,
		/**
		 * The specified component factory method is inaccessible to WicketWiz
		 * (the SecurityManager throws a SecurityException).
		 */
		FACTORY_METHOD_INACCESSIBLE,
		INJECTION_POINT_IS_NOT_FRAGMENTBUILDER,
		/**
		 * The injection target's class is abstract, and the injector could
		 * not find a suitable implementation. The injector may have access
		 * to providers which supply implementations of well known abstract
		 * Wicket component classes.
		 */
		ABSTRACT_CLASS_WITHOUT_IMPLEMENTATION,
		
		/**
		 * The item class specified on the injection point is not compatible
		 * with the item class of its target type.
		 */
		ITEMCLASS_NOT_ASSIGNABLE_REPEATER_ITEMCLASS,
		
		/**
		 * An item class was specified on the injection point, but the 
		 * injection point's type is not a repeater. An item class may only
		 * be specified on injection points that receive subclasses of
		 * AbstractRepeater.
		 */
		ITEMCLASS_ON_NON_REPEATER_INJECTION_POINT,
	}
	
	Type getType();
	
	String formatMessage();
}
