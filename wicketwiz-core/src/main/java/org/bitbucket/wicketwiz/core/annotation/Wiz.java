/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.wicketwiz.core.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.list.Loop;
import org.apache.wicket.markup.repeater.AbstractRepeater;
import org.apache.wicket.markup.repeater.Item;

/**
 * Wiz-enabled Wicket components mark component injection points with this
 * annotation.
 * @author count
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface Wiz {
	String factoryMethod() default "";
	
	/**
	 * If the injection point is a subclass of {@link AbstractRepeater}, itemClass
	 * specifies the item type that should be used. For {@link ListView}, this
	 * should be a subclass of {@link ListItem}, for {@link Loop}, this should
	 * be a subclass of {@link Item}. If {@link MarkupContainer} is specified (the default),
	 * the repeater class's default itemClass will be used (e.g. {@link ListItem}
	 * for {@link ListView}.
	 * @return	The itemClass specified for this injection point or MarkupContainer, if
	 *	the repeater's default item class should be usesd.
	 */
	Class<? extends MarkupContainer> itemClass() default MarkupContainer.class;
}
