/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.wicketwiz.core.model;

import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author upachler
 */
public class ClassName implements Comparable<ClassName>{
	private final String packageName;
	private final String simpleName;

	private static final Pattern CLASS_NAME_PATTERN = Pattern.compile("((?:\\w+\\.)*)([^\\.]+)\\z");
	
	public ClassName(String packageName, String simpleName) {
		this.packageName = packageName;
		this.simpleName = simpleName;
		if(packageName == null || simpleName == null) {
			throw new NullPointerException("neither package name nor simple name may be null");
		}
	}

	public String getPackageName() {
		return packageName;
	}

	public String getSimpleName() {
		return simpleName;
	}
	
	public String getName() {
		return !packageName.isEmpty() ? (packageName + '.' + simpleName) : simpleName;
	}

	@Override
	public int hashCode() {
		int hash = 7;
		hash = 29 * hash + Objects.hashCode(this.packageName);
		hash = 29 * hash + Objects.hashCode(this.simpleName);
		return hash;
	}

	@Override
	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		final ClassName other = (ClassName) obj;
		if (!Objects.equals(this.packageName, other.packageName)) {
			return false;
		}
		if (!Objects.equals(this.simpleName, other.simpleName)) {
			return false;
		}
		return true;
	}

	public int compareTo(ClassName o) {
		int cmp  = packageName.compareTo(o.packageName);
		if(cmp != 0) {
			return cmp;
		}
		return simpleName.compareTo(o.simpleName);
	}
	
	public static ClassName valueOf(String s) {
		Matcher m = CLASS_NAME_PATTERN.matcher(s);
		if(!m.matches()) {
			throw new IllegalArgumentException(String.format("The given name '%s' is not a valid class name", s));
		}
		String packageName = m.group(1);
		if(!packageName.isEmpty()) {
			packageName = packageName.substring(0, packageName.length()-1);
		}
		String simpleName = m.group(2);
		return new ClassName(packageName, simpleName);
	}
}
