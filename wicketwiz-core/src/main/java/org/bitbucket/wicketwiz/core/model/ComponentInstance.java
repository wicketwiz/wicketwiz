/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.wicketwiz.core.model;

import java.util.Map;

/**
 *
 * @author upachler
 */
public interface ComponentInstance extends Compound {
	
	String getWicketId();
	ClassName getComponentClassName();
	
	ComponentInstance getParentInstance();

	public boolean isHtmlElement(String elementName);
	
	public boolean isWicketElement(String elementName);
	
	public Map<String,String> getHtmlElementAttributes();
	
}
