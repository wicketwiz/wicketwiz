/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.wicketwiz.core.introspector;

import org.apache.wicket.MarkupContainer;
import org.bitbucket.wicketwiz.core.model.HostCompound;

/**
 *
 * @author count
 */
public interface ComponentInjector {
	
	void validate(Class<? extends MarkupContainer> containerClass, HostCompound markupModel) throws InjectorValidationException;
	
	void injectComponents(MarkupContainer component, HostCompound markupModel)throws InjectorValidationException;
}
