/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.wicketwiz.core.parser;

/**
 *
 * @author count
 */
public class PageParserException extends Exception{
	private ParserError[] errors;
	
	public PageParserException(ParserError[] errors) {
		super(toString(errors));
		this.errors = errors;
	}
	
	public ParserError[] getErrors() {
		return errors;
	}
	
	private static String toString(ParserError[] errors) {
		StringBuilder sb = new StringBuilder();
		for(ParserError e : errors) {
			if(sb.length()!=0) {
				sb.append("\n");
			}
			sb.append(String.format("Error: '%s'; Location %s", e.formatMessage(), e.getLocation()));
		}
		return sb.toString();
	}
}
