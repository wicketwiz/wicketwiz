/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.wicketwiz.core.codegen;

import java.io.IOException;
import org.bitbucket.wicketwiz.core.model.HostCompound;

/**
 *
 * @author upachler
 */
public interface CodeGenerator {
	void generate(HostCompound hostCompound) throws IOException;
}
