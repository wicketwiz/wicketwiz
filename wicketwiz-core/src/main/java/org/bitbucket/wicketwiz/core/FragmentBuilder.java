/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.wicketwiz.core;

import org.apache.wicket.markup.html.panel.Fragment;

/**
 * Creates a new instance of T and injects associated components into it.
 * A FragmentBuilder should be used as injection points that match
 * a wicket:id on a wicket:fragment element. Because fragments are
 * not intantiated when the host container is created, but on-demand,
 * the FragmentBuilder can be called by client code to create Fragments
 * on the fly.
 */
public interface FragmentBuilder<T extends Fragment> {
	public T build(String wicketId);
}
