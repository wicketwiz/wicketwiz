/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.wicketwiz.core;

import java.net.URI;
import java.util.Iterator;
import java.util.ServiceLoader;
import static org.bitbucket.wicketwiz.core.WicketWizFactory.newInstance;
import org.bitbucket.wicketwiz.core.codegen.CodeGenerator;
import org.bitbucket.wicketwiz.core.introspector.ComponentInjector;
import org.bitbucket.wicketwiz.core.parser.PageParser;

/**
 * This abstract class acts as bootstrapper for the WicketWiz implementation.
 * As a regular user, you'll probably won't need this class, as the tooling
 * and runtime library will handle all this for you.
 * <p>WicketWizFactory uses the {@link java.util.ServiceLoader} to load a WicketWiz implementation.
 * Because in WicketWiz the core API and its implementation are strictly separated,
 * it is not possible to acquire the implementation directly. Instead, the
 * WicketWizFactory class and its statie {@link #newInstance() } methods
 * are used to obtain a factory implementation, which then instantiates
 * all other WicketWiz components.</p>
 * 
 * <p>Making a long story short, this is what you need to do to get a PageParser
 * or ComponentInjector:</p>
 * 
 * <pre>
 * WicketWizFactory factory = WicketWizFactory.newInstance();
 * 
 * PageParser pageParser = factory.createPageParser();
 * 
 * ComponentInjector = factory.createComponentInjector();
 * </pre>
 * @author upachler
 */
public abstract class WicketWizFactory {
	
	/**
	 * Create a new WicketWizFactory implementation using the current thread's
	 * context class loader.
	 * @return A new factory instance
	 */
	public static WicketWizFactory newInstance() {
		return newInstance(Thread.currentThread().getContextClassLoader());
	}
	
	/**
	 * Create a new WicketWizFactory implementation with the provided class
	 * loader. 
	 * @param classLoader	The class loader to use for locating the
	 *	factory implementation.
	 * @return A new factory instance
	 */
	public static WicketWizFactory newInstance(ClassLoader classLoader) {
		Iterator<WicketWizFactory> serviceIt = ServiceLoader.load(WicketWizFactory.class, classLoader).iterator();
		if(serviceIt.hasNext()) {
			// return first implementation we find
			return serviceIt.next();
		} else {
			return null;
		}
	}
	
	/**
	 * Creates a new CodeGenerator
	 * @param outputBaseUri This URI defines the root location where the
	 *	code generator's output will be written to.
	 * @return a new CodeGenerator provided by the WicketWiz implementation
	 */
	public abstract CodeGenerator createCodeGenerator(URI outputBaseUri);
	
	/**
	 * Creates a new PageParser with the given base URI. 
	 * @param inputBaseURI	The base URI. All pages parsed by this
	 * parser must be relative to this URI.
	 * @return	a new PageParser instance, provided by the WicketWiz implementation.
	 */
	public abstract PageParser createPageParser(URI inputBaseURI);

	public abstract ComponentInjector createInjector();
}
