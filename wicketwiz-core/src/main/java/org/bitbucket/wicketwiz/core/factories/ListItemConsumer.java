/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.wicketwiz.core.factories;

/**
 * Repeaters using ListItem instances such as ListView, PropertyListView etc.
 * need to implement this interface to enable component injection to their
 * ListItems.
 */
public interface ListItemConsumer<T> {
	void setListItemFactory(ListViewItemFactory<T> listItemFactory);
}
