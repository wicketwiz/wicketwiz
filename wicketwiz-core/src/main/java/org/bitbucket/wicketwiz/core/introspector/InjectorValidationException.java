/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.wicketwiz.core.introspector;

/**
 *
 * @author count
 */
public class InjectorValidationException extends Exception {
	private final InjectorError[] errors;
	
	public InjectorError[] getErrors() {
		return errors;
	}

	public InjectorValidationException(InjectorError[] errors) {
		this.errors = errors;
	}

	@Override
	public String getMessage() {
		StringBuilder sb = new StringBuilder("Errors were detected: \n");
		for(InjectorError error : errors) {
			sb.append(error.getType());
			sb.append(": ");
			sb.append(error.formatMessage());
			sb.append('\n');
		}
		return sb.toString();
	}
	
}
