/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.wicketwiz.core.factories;

import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.model.IModel;

/**
 *
 * @author upachler
 */
public interface ListViewItemFactory<T> {
	ListItem<T> create(int index, IModel<T> model);
}
