/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.wicketwiz.core.parser;

import java.io.IOException;
import java.net.URI;
import org.bitbucket.wicketwiz.core.WicketWizFactory;
import org.bitbucket.wicketwiz.core.model.HostCompound;

/**
 * Instances of PageParser will analyse XHTML pages as used by Apache Wicket and
 * scan them for WicketWiz attributes. If it finds them, the parser will generate
 * a {@link HostCompound}, which describes the Wicket component structure
 * inherent in that page, as described by the Wicket tags and attributes and
 * the supporting WicketWiz tags.
 * 
 * Note that if WicketWiz is activated in the XHTML page, parsing the page
 * may fail if WicketWiz attributes required to fully describe the Wicket
 * Component structure are missing.
 * 
 * @author upachler
 */
public interface PageParser {
	
	/**
	 * This method will scan the XHTML file at the given URI for WicketWiz
	 * XML attributes and generate a {@link HostCompound} if it finds them.
	 * If there are no WicketWiz attributes in the page, null will be
	 * returned.
	 * @param sourceURI	the source URI; this must be relative to the 
	 *	base URI ({@link #getBaseUri()})
	 * @return	the compound representing the components in the sourceURI's
	 *	markup, or null if WicketWiz was not enabled in the markup.
	 * @throws PageParserException	the PageParser detected a syntactic problem 
	 *	while parsing the markup the source URI
	 * @throws IOException	an error occurred while reading from the source URI.
	 */
	HostCompound parsePage(URI sourceURI) throws PageParserException, IOException;
	
	/**
	 * The base URI used by this instance. The base URI cannot be changed and
	 * is specified when the PageParser is created.
	 * 
	 * @see WicketWizFactory#createPageParser(java.net.URI) 
	 * @return the base URI used by this instance
	 */
	URI getBaseUri();
}
