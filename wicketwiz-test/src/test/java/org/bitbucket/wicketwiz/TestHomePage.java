package org.bitbucket.wicketwiz;

import org.apache.wicket.util.tester.WicketTester;
import org.bitbucket.wicketwiz.addressbook.Addressbook;
import org.bitbucket.wicketwiz.fragment.Index;
import org.junit.Before;
import org.junit.Test;
import org.bitbucket.wicketwiz.guestbook.GuestBook;
import org.bitbucket.wicketwiz.navomatic.Page1;

/**
 * Simple test using the WicketTester
 */
public class TestHomePage
{
	private WicketTester tester;

	@Before
	public void setUp()
	{
		tester = new WicketTester(new WicketApplication());
	}

	@Test
	public void homepageRendersSuccessfully()
	{
		//start and render the test page
		tester.startPage(HomePage.class);

		//assert rendered page class
		tester.assertRenderedPage(HomePage.class);
	}
	
	@Test
	public void testNavomatic() {
		tester.startPage(Page1.class);
		
		tester.assertRenderedPage(Page1.class);
	}
	
	@Test
	public void testGuestBook() {
		tester.startPage(GuestBook.class);
		
		tester.assertRenderedPage(GuestBook.class);
	}
	
	@Test
	public void testIndex() {
		tester.startPage(Index.class);
		
		tester.assertRenderedPage(Index.class);
	}
	
	@Test
	public void testAddressBook() {
		tester.startPage(Addressbook.class);
		
		tester.assertRenderedPage(Addressbook.class);
	}
}
