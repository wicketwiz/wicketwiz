package org.bitbucket.wicketwiz;

import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.Model;
import org.bitbucket.wicketwiz.core.annotation.Wiz;

public class HomePage extends WebPage {
	private static final long serialVersionUID = 1L;
	
	@Wiz
	Label version;
	
	@Wiz
	TextField textinput;
	
	@Wiz(factoryMethod="createOtherLabel")
	Label otherLabel;
	
	Label createOtherLabel(String id) {
		return new Label(id, "this is the other label");
	}
	
    public HomePage(final PageParameters parameters) {
		super(parameters);
		version.setDefaultModel(Model.of("1.2.3"));
    }
}
