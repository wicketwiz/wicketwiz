package org.bitbucket.wicketwiz;

import org.apache.wicket.protocol.http.WebApplication;
import org.bitbucket.wicketwiz.addressbook.Addressbook;
import org.bitbucket.wicketwiz.fragment.Index;
import org.bitbucket.wicketwiz.guestbook.GuestBook;
import org.bitbucket.wicketwiz.navomatic.Page1;
import org.bitbucket.wicketwiz.runtime.WicketWizComponentInjector;

/**
 * Application object for your web application. If you want to run this application without deploying, run the Start class.
 * 
 * @see org.bitbucket.wicketwiz.Start#main(String[])
 */
public class WicketApplication extends WebApplication
{    	
	/**
	 * @see org.apache.wicket.Application#getHomePage()
	 */
	@Override
	public Class<HomePage> getHomePage()
	{
		return HomePage.class;
	}

	/**
	 * @see org.apache.wicket.Application#init()
	 */
	@Override
	public void init()
	{
		super.init();
		
		getComponentInstantiationListeners().add(new WicketWizComponentInjector(this));
		mountPackage("navomatic", Page1.class);
		mountPackage("guestbook", GuestBook.class);
		mountPackage("fragment", Index.class);
		mountPackage("addressbook", Addressbook.class);
	}
}
