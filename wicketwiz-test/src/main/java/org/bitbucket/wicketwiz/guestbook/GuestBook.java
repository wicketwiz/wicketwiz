/*
 * Copyright 2015 count.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.wicketwiz.guestbook;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.basic.MultiLineLabel;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextArea;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.PropertyListView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.util.ListModel;
import org.apache.wicket.util.value.ValueMap;
import org.bitbucket.wicketwiz.core.annotation.Wiz;


public final class GuestBook extends WebPage {
    /** A global list of all comments from all users across all sessions */
    private static final List<Comment> commentList = Collections.synchronizedList(new ArrayList<Comment>());
	
	@Wiz
	CommentForm commentForm;
	
	@Wiz
	CommentPropertyListView comments;
	
	@Wiz
	TextArea<String> commentText;
	
	@Wiz
	TextField<String> commentField;
	
	static class CommentPropertyListView extends PropertyListView<Comment> {

		public CommentPropertyListView(String id) {
			super(id);
		}

		@Override
		protected void populateItem(ListItem<Comment> listItem) {
			listItem.add(new Label("date"));
			listItem.add(new MultiLineLabel("text"));
		}
		
	}
    /**
     * Constructor that is invoked when page is invoked without a session.
     */
    public GuestBook() {
		commentForm.setModel(new CompoundPropertyModel<ValueMap>(new ValueMap()));
		commentForm.setMarkupId("commentForm");	// the original example says: 'this is just to make the unit test happy'
		
        // Add commentListView of existing comments
        comments.setModel(new ListModel(commentList));
		comments.setVersioned(false);
		
		commentText.setType(String.class);
		commentField.setType(String.class);
    }

    /**
     * A form that allows a user to add a comment.
     */
    public final class CommentForm extends Form<ValueMap> {
        public CommentForm(final String id) {
			super(id);
        }

        /**
         * Show the resulting valid edit
         */
        @Override
        public final void onSubmit() {
			commentFormSubmit();
		}
		
	}
	
	private void commentFormSubmit() {
		ValueMap values = commentForm.getModelObject();

		// check if the honey pot is filled
		Object commentString = values.get("comment");
		if (commentString!=null && !commentString.toString().trim().isEmpty()) {
			error("Caught a spammer!!!");
			return;
		}
		// Construct a copy of the edited comment
		Comment comment = new Comment();

		// Set date of comment to add
		comment.setDate(new Date());
		comment.setText((String)values.get("text"));
		commentList.add(0, comment);

		// Clear out the text component
		values.put("text", "");
	}
}