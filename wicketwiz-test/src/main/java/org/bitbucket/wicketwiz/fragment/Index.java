/*
 * Copyright 2015 count.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.wicketwiz.fragment;

import org.apache.wicket.MarkupContainer;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.Loop;
import org.apache.wicket.markup.html.list.LoopItem;
import org.apache.wicket.markup.html.panel.Fragment;
import org.bitbucket.wicketwiz.core.FragmentBuilder;
import org.bitbucket.wicketwiz.core.annotation.Wiz;

public class Index extends WebPage {
	@Wiz
	FragmentBuilder<Fragment1> fragment1;
	@Wiz
	FragmentBuilder<Fragment> fragment2;
	@Wiz(factoryMethod = "makeLoop")
	Loop list;
	
    public class Fragment1 extends Fragment {
		@Wiz(factoryMethod = "makeLabel")
		Label label;
        public Fragment1(String id, String markupId, MarkupContainer markupProvider) {
            super(id, markupId, markupProvider);
		}
		Label makeLabel(String id) {
            return new Label(id, "Hello, World!");
        }
    }

	Loop makeLoop(String wicketId) {
        return new Loop(wicketId, 5) {
            protected void populateItem(LoopItem item) {
                int index = (item.getIndex() % 2 + 1);
                if (index == 1) {
                    item.add(fragment1.build("panel"));
                } else {
                    item.add(fragment2.build("panel"));
                }
            }
        };
    }
}
