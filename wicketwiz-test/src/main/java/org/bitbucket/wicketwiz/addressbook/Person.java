/*
 * Copyright 2015 upachler.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.wicketwiz.addressbook;

/**
 *
 * @author uwe_pachler
 */
public class Person {
	final String firstName;
	final String surname;

	public Person(String firstName, String surname) {
		this.firstName = firstName;
		this.surname = surname;
	}


	public String getFirstName() {
		return firstName;
	}

	public String getSurname() {
		return surname;
	}

}

