/*
 * Copyright 2015 upachler.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.wicketwiz.addressbook;

import java.util.Arrays;
import java.util.List;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.IModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.util.ListModel;
import org.bitbucket.wicketwiz.core.annotation.Wiz;

/**
 *
 * @author upachler
 */
public class Addressbook extends WebPage {
	class PersonItem extends ListItem {
		public PersonItem(int index, IModel<Person> model) {
			super(index, model);
			firstName.setDefaultModel(new PropertyModel<String>(model, "firstName"));
			surname.setDefaultModel(new PropertyModel<String>(model, "surname"));
		}
		
		@Wiz
		Label firstName;
		
		@Wiz
		Label surname;
	}
	
	@Wiz(itemClass = PersonItem.class)
	ListView<Person> personListView;
	
	List<Person> persons = Arrays.asList(
		new Person("Patrick", "Mühleck"),
		new Person("Marc", "Bräuer"),
		new Person("Thorsten", "Haberecht"),
		new Person("Uwe", "Pachler")
	);
	public Addressbook() {
		personListView.setModel(new ListModel(persons));
	}
	
	
}
