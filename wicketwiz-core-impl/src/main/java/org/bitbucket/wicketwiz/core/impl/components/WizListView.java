/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.wicketwiz.core.impl.components;

import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.IModel;
import org.bitbucket.wicketwiz.core.factories.ListItemConsumer;
import org.bitbucket.wicketwiz.core.factories.ListViewItemFactory;

/**
 * A concrete ListView implementation, which the component injector may
 * use directly for injection points that use the abstract ListView<T> type
 * as target type.
 */
public class WizListView<T> extends ListView<T> implements ListItemConsumer<T>{
	private ListViewItemFactory<T> listItemFactory;

	public WizListView(String id) {
		super(id);
	}

	@Override
	protected ListItem<T> newItem(int index, IModel itemModel) {
		return listItemFactory.create(index, itemModel);
	}
	
	
	public void setListItemFactory(ListViewItemFactory<T> listItemFactory) {
		this.listItemFactory = listItemFactory;
	}
	
	@Override
	protected void populateItem(ListItem item) {
		// do nothing; the component injector will inject whatever it needs
		// into the ListItem implementation
	}
	
}
