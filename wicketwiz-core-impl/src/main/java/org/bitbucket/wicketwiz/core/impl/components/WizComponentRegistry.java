/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.wicketwiz.core.impl.components;

import java.lang.reflect.Method;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.list.Loop;
import org.apache.wicket.markup.repeater.AbstractRepeater;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.IModel;

/**
 * A registry for component classes. For now, we only have internally issued
 * component classes hard-coded here, but we can easily extend this scheme
 * so that users can provide their own registries.
 */
public class WizComponentRegistry {

	public <T> Class<? extends T> findImplementation(Class<T> clazz) {
		if(ListView.class.isAssignableFrom(clazz)) {
			return WizListView.class.asSubclass(clazz);
		} else {
			return null;
		}
	}

	public Class<? extends MarkupContainer> findDefaultItemClass(Class<? extends AbstractRepeater> repeaterClass) {
		Class[] parameterTypes;
		if(ListView.class.isAssignableFrom(repeaterClass)) {
			parameterTypes = new Class[]{int.class, IModel.class};
		} else if (Loop.class.isAssignableFrom(repeaterClass)) {
			parameterTypes = new Class[]{int.class};
		} else if (RepeatingView.class.isAssignableFrom(repeaterClass)) {
			parameterTypes = new Class[]{String.class, int.class, IModel.class};
		} else {
			return MarkupContainer.class;
		}
		Class clazz = repeaterClass;
		while(clazz != null) {
			try {
				Method m = clazz.getDeclaredMethod("newItem", parameterTypes);
				return m.getReturnType().asSubclass(MarkupContainer.class) ;
			} catch (NoSuchMethodException ex) {
				// try superclass
				clazz = clazz.getSuperclass();
			}
		}
		throw new RuntimeException(String.format("Method 'newItem()' (%d parameters) not found", parameterTypes.length));
	}
	
}
