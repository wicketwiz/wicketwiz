/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.wicketwiz.core.impl;

import java.util.Collections;
import java.util.Map;
import org.bitbucket.wicketwiz.core.model.ClassName;
import org.bitbucket.wicketwiz.core.model.ComponentInstance;

/**
 *
 * @author count
 */
public class ComponentInstanceImpl extends AbstractCompoundImpl implements ComponentInstance {
	
	enum ElementType {
		XHTML,
		WICKET
	}
	private ComponentInstance parentInstance;
	private ClassName componentClassName;
	
	private final String wicketId;
	private final ElementType elementType;
	private final String elementName;
	private final Map<String,String> htmlElementAttributes;

	public ComponentInstanceImpl(String wicketId, ElementType elementType, String elementName) {
		this(wicketId, elementType, elementName, Collections.<String,String>emptyMap());
	}
	
	public ComponentInstanceImpl(String wicketId, ElementType elementType, String htmlElementName, Map<String,String> htmlElementAttributes) {
		this.wicketId = wicketId;
		this.elementType = elementType;
		this.elementName = htmlElementName;
		this.htmlElementAttributes = htmlElementAttributes;
	}
	
	
	@Override
	public String getWicketId() {
		return wicketId;
	}

	@Override
	public ClassName getComponentClassName() {
		return componentClassName;
	}

	public ComponentInstance getParentInstance() {
		return parentInstance;
	}

	public boolean isHtmlElement(String elementName) {
		return elementType == ElementType.XHTML && this.elementName.equals(elementName);
	}

	public boolean isWicketElement(String elementName) {
		return elementType == ElementType.WICKET && this.elementName.equals(elementName);
	}

	public Map<String, String> getHtmlElementAttributes() {
		return htmlElementAttributes;
	}

	
	void setParentInstance(ComponentInstance parentInstance) {
		this.parentInstance = parentInstance;
	}

	void setComponentClassName(ClassName componentClassName) {
		this.componentClassName = componentClassName;
	}
	
	
}
