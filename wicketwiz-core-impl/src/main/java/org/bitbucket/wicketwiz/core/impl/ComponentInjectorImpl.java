/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.wicketwiz.core.impl;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import org.apache.wicket.Component;
import org.apache.wicket.MarkupContainer;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Fragment;
import org.apache.wicket.markup.repeater.AbstractRepeater;
import org.apache.wicket.model.IModel;
import org.bitbucket.wicketwiz.core.FragmentBuilder;
import org.bitbucket.wicketwiz.core.annotation.Wiz;
import org.bitbucket.wicketwiz.core.factories.ListItemConsumer;
import org.bitbucket.wicketwiz.core.factories.ListViewItemFactory;
import org.bitbucket.wicketwiz.core.impl.components.WizComponentRegistry;
import org.bitbucket.wicketwiz.core.introspector.ComponentInjector;
import org.bitbucket.wicketwiz.core.introspector.InjectorError;
import org.bitbucket.wicketwiz.core.introspector.InjectorValidationException;
import org.bitbucket.wicketwiz.core.model.ClassName;
import org.bitbucket.wicketwiz.core.model.ComponentInstance;
import org.bitbucket.wicketwiz.core.model.Compound;
import org.bitbucket.wicketwiz.core.model.HostCompound;

/**
 *
 * @author count
 */
public class ComponentInjectorImpl implements ComponentInjector {
	
	private static final WizComponentRegistry componentRegistry = new WizComponentRegistry();
	private static final Class<AbstractRepeater> REPEATER_CLASS = AbstractRepeater.class;

	private final ThreadLocal<Recipe> nextComponentRecipeTL = new ThreadLocal<Recipe>();

	static class HierarchyBuilderCmd {
		enum Type {
			SET_PARENT,
			ADD_TO_PARENT
		};
		final Type type;
		final String wicketId;

		public HierarchyBuilderCmd(Type type, String wicketId) {
			this.type = type;
			this.wicketId = wicketId;
		}
		
	}
	static class Recipe {
		private final Map<String, InjectionPoint> wicketIdToInjectionPointMap;
		private final List<Injectable> injectables;
		private final List<HierarchyBuilderCmd> hierarchyBuilderCommands;
		
		public Recipe(Map<String, InjectionPoint> wicketIdToInjectionPointMap, List<Injectable> injectables, List<HierarchyBuilderCmd> hierarchyBuilderCommands) {
			this.wicketIdToInjectionPointMap = wicketIdToInjectionPointMap;
			this.injectables = injectables;
			this.hierarchyBuilderCommands = hierarchyBuilderCommands;
		}

		public Map<String, InjectionPoint> getWicketIdToInjectionPointMap() {
			return wicketIdToInjectionPointMap;
		}

		public List<Injectable> getInjectables() {
			return injectables;
		}
		
		
	}

	private class ItemConsumerFactory<T extends ListView & ListItemConsumer> implements Factory {
		private final Factory<T> delegate;
		private final ConstructorFactoryBase<? extends ListItem> itemFactory;
		private final Recipe itemRecipe;

		public ItemConsumerFactory(Factory<T> factory, Constructor<ListItem> itemConstructor, Recipe itemRecipe) {
			this.delegate = factory;
			this.itemFactory = new ConstructorFactoryBase<ListItem>(itemConstructor);
			this.itemRecipe = itemRecipe;
		}

		public T create(final MarkupContainer container, String name) {
			T instance = delegate.create(container, name);
			instance.setListItemFactory(new ListViewItemFactory<T>() {
				public ListItem<T> create(int index, IModel<T> model){
					ComponentInjectorImpl.this.setNextComponentRecipe(itemRecipe);
					return itemFactory.newInstance(container, index, model);
				}
			});
			return instance;
		}

		public Class getType() {
			throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
		}
	}
	
	interface Factory<T> {
		T create(MarkupContainer container, String name);

		Class<?> getType();
	}
	
	interface ComponentFactory<T extends Component> extends Factory<T> {
		T create(MarkupContainer container, String name);
	}
	
	private static class ConstructorComponentFactory<T extends Component> extends ConstructorFactoryBase<T> implements ComponentFactory<T> {

		public ConstructorComponentFactory(Constructor constructor) {
			super(constructor);
		}
		
		public T create(final MarkupContainer container, String name) {
			return newInstance(container, name);
		}
	}
	
	private static class ConstructorFactoryBase<T> {
		private final Constructor<T> constructor;
		private final boolean isInnerClassConstructor;

		public ConstructorFactoryBase(Constructor<T> constructor) {
			this.constructor = constructor;
			Class declaringClass = constructor.getDeclaringClass();
			this.isInnerClassConstructor = declaringClass.getEnclosingClass() != null && !Modifier.isStatic(declaringClass.getModifiers());
		}
		
		protected T newInstance(Object enclosingInstance, Object... parameters) {
			Object[] actualParameters;
			if(isInnerClassConstructor) {
				actualParameters = new Object[parameters.length+1];
				actualParameters[0] = enclosingInstance;
				System.arraycopy(parameters, 0, actualParameters, 1, parameters.length);
			} else {
				actualParameters = parameters;
			}
			try {
				T f = constructor.newInstance(actualParameters);
				return f;
			} catch (InstantiationException ex) {
				throw new RuntimeException(ex);
			} catch (IllegalAccessException ex) {
				throw new RuntimeException(ex);
			} catch (InvocationTargetException ex) {
				throw new RuntimeException(ex);
			}
		}
		
		public Class<?> getType() {
			return constructor.getDeclaringClass();
		}
	}
	
	private class FragmentBuilderFactory<T extends Fragment> extends ConstructorFactoryBase<T> implements Factory<FragmentBuilder<T>>{
		final String fragmentId;
		private final Recipe recipe;

		public FragmentBuilderFactory(Constructor<T> fragmentConstructor, String fragmentId, Recipe recipe) {
			super(fragmentConstructor);
			this.fragmentId = fragmentId;
			this.recipe = recipe;
		}
		
		
		public FragmentBuilder<T> create(final MarkupContainer container, final String name) {
			return new FragmentBuilder<T>() {

				public T build(String wicketId) {
					ComponentInjectorImpl.this.setNextComponentRecipe(recipe);
					T f = newInstance(container, wicketId, fragmentId, container);
					return f;
				}
				
			};
		}
	}
	
	private static class FactoryMethodComponentFactory<T extends Component> implements ComponentFactory<T> {
		private final Method factoryMethod;
		FactoryMethodComponentFactory(Method factoryMethod) {
			this.factoryMethod = factoryMethod;
		}

		public T create(MarkupContainer container, String name) {
			try {
				return (T) factoryMethod.invoke(container, name);
			} catch (IllegalAccessException ex) {
				throw new RuntimeException(ex);
			} catch (IllegalArgumentException ex) {
				throw new RuntimeException(ex);
			} catch (InvocationTargetException ex) {
				throw new RuntimeException(ex);
			}
		}
		
		public Class<?> getType() {
			return factoryMethod.getReturnType();
		}
	}
	
	private static class Injectable<T> {
		private final InjectionPoint injectionPoint;
		private final Factory<T> factory;

		public Injectable(InjectionPoint injectionPoint, Factory<T> factory) {
			this.injectionPoint = injectionPoint;
			this.factory = factory;
		}

		private Object inject(MarkupContainer container) {
			Object instance = factory.create(container, injectionPoint.getPropertyName());
			injectionPoint.inject(container, instance);
			return instance;
		}
		
	}
	
	private static class InjectionPointImpl implements InjectionPoint {
		
		private final Field field;
		private final Method factoryMethod;
		private final Class itemClass;
		
		public InjectionPointImpl(Field field, Method factoryMethod, Class itemClass) {
			this.field = field;
			this.factoryMethod = factoryMethod;
			this.itemClass = itemClass;
		}
		
		public String getPropertyName() {
			return field.getName();
		}

		public void inject(MarkupContainer container, Object componentToInject) {
			try {
				field.set(container, componentToInject);
			} catch (IllegalAccessException ex) {
				throw new RuntimeException(ex);
			}
		}

		public Class<?>[] getTypeArguments() {
			ParameterizedType genericType = (ParameterizedType)field.getGenericType();
			Type[] types = genericType.getActualTypeArguments();
			Class<?>[] typeArgs = new Class[types.length];
			System.arraycopy(types, 0, typeArgs, 0, types.length);
			return typeArgs;
		}
		
		public Class<?> getType() {
			Class<?> type = field.getType();
			return type;
		}

		public Method getFactoryMethod() {
			return factoryMethod;
		}
		
		public Class<? extends MarkupContainer> getItemClass() {
			return itemClass;
		}
		
	}
	
	private Map<String,InjectionPoint> findInjectionPoints(Class <? extends MarkupContainer> componentClass, List<InjectorError> errors) {
		Map<String,Field> fieldMap = new HashMap<String,Field>();
		
		for(Class clazz=componentClass; clazz!=null; clazz=clazz.getSuperclass()) {
			Field[] fields = clazz.getDeclaredFields();
			for(Field field : fields) {
				if(fieldMap.containsKey(field.getName())) {
					continue;
				}
				
				Wiz wizAnnotation = field.getAnnotation(Wiz.class);
				if(wizAnnotation == null) {
					continue;
				}
				
				field.setAccessible(true);

				fieldMap.put(field.getName(), field);
			}
		}
		
		Map<String,InjectionPoint> injectionPoints = new HashMap<String,InjectionPoint>(fieldMap.size());
		for(Field field : fieldMap.values()) {
			Wiz wiz = field.getAnnotation(Wiz.class);
			Method factoryMethod = null;
			Class<? extends MarkupContainer> itemClass = wiz.itemClass();
			if(itemClass == MarkupContainer.class) {
				itemClass = null;
			}
			if(!wiz.factoryMethod().isEmpty()) {
				try {
					Method m = componentClass.getDeclaredMethod(wiz.factoryMethod(), String.class);
					m.setAccessible(true);
					if(field.getType().isAssignableFrom(m.getReturnType())) {
						factoryMethod = m;
					} else {
						error(errors, InjectorError.Type.FACTORY_METHOD_RETURN_TYPE_INCOMPATIBLE, "The return type of specified factoryMethod '%s' in field '%s' is not assignment compatible with the field's type", wiz.factoryMethod(), field);
					}
				} catch(NoSuchMethodException nsmx) {
					error(errors, InjectorError.Type.FACTORY_METHOD_NOT_FOUND, "The declared factoryMethod '%s' cannot be found in class '%s'", wiz.factoryMethod(), componentClass.getName());
				} catch(SecurityException sx) {
					error(errors, InjectorError.Type.FACTORY_METHOD_INACCESSIBLE, "Cannot access factoryMethod '%s' of class '%s'", wiz.factoryMethod(), componentClass.getName());
				}
			}
			InjectionPoint ij = new InjectionPointImpl(field, factoryMethod, itemClass);
			injectionPoints.put(ij.getPropertyName(), ij);
		}
		
		return injectionPoints;
	}
	
	private Recipe acquireRecipe(Class<? extends MarkupContainer> containerClass, Compound markupModel) throws InjectorValidationException {
		List<InjectorError> errors = new ArrayList<InjectorError>();
		
		Recipe recipe = acquireRecipeImpl(containerClass, markupModel, errors);
		
		if(!errors.isEmpty()) {
			throw new InjectorValidationException(errors.toArray(new InjectorError[errors.size()]));
		}
		return recipe;
	}
		
	private Recipe acquireRecipeImpl(Class<? extends MarkupContainer> containerClass, Compound markupModel, List<InjectorError> errors) {
		
		// find all injection points (spots in the MarkupContainer instance
		// that carry the {@link Wiz} annotation,
		Map<String, InjectionPoint> injectionPoints = findInjectionPoints(containerClass, errors);

		Map<String, ComponentInstance> componentInstances = new TreeMap<String,ComponentInstance>();
		ModelUtils.findComponentInstances(markupModel, componentInstances);

		// MORE VALIDATION!
		// at the least, there must be an injection point for every ComponentInstance
		Set<String> missingInstanceIds = new HashSet<String>(componentInstances.keySet());
		missingInstanceIds.removeAll(injectionPoints.keySet());

		// check types
		Set<String> remainingInstanceIds = new HashSet<String>(componentInstances.keySet());
		remainingInstanceIds.removeAll(missingInstanceIds);

		List<Injectable> injectables = new ArrayList<Injectable>();
		
		for(InjectionPoint ij : injectionPoints.values()) {
			
			String wicketId = ij.getPropertyName();
			ComponentInstance ci = componentInstances.get(wicketId);
			if(ci == null) {
				error(errors, InjectorError.Type.MISSING_COMPONENT_INSTANCE, "There is no markup with wicket:id='%s', but an injection point exists with this name", wicketId);
				continue;
			}
			Injectable injectable = prepareInjectable(containerClass, ij, ci, errors);
			if(injectable != null) {
				injectables.add(injectable);
			}
		}
		
		List<HierarchyBuilderCmd> commands = new ArrayList<HierarchyBuilderCmd>(injectables.size());
		Set<String> ignoredWicketIds = new HashSet<String>();
		buildHierarchyCommands(containerClass, injectionPoints, markupModel.getChildren(), null, commands, errors, ignoredWicketIds);
		
		return new Recipe(injectionPoints, injectables, commands);
	}
	
	
	Injectable prepareInjectable(Class containerClass, InjectionPoint ij, ComponentInstance ci, List<InjectorError> errors) {
		Class<?> clazz;
		// if a component class was specified, check that it can be assigned
		// to the injection point
		if(ci.getComponentClassName() != null) {

			// FIXME: error checking for unresolvable classes
			Class ciClass;
			ClassName ciClassName = ci.getComponentClassName();
			try {
				ciClass = Class.forName(ciClassName.getName());
			} catch (ClassNotFoundException ex) {
				error(errors, InjectorError.Type.COMPONENT_CLASS_NOT_FOUND, "The class '%s' as referenced in markup wicket:id='%s' cannot be found", ciClassName, ci.getWicketId());
				return null;
			}
			try {
				clazz = ciClass.asSubclass(Component.class);
			} catch (ClassCastException ccx) {
				error(errors, InjectorError.Type.COMPONENT_INSTANCE_CLASS_NOT_COMPONENT, "The class '%s' is not a subclass of '%s'", ciClassName, Component.class);
				return null;
			}

			try {
				clazz = clazz.asSubclass(ij.getType());
			} catch(ClassCastException ccx) {
				error(errors, InjectorError.Type.INCOMPATIBLE_FIELD_TYPE, "The component '%s' of cannot be injected into the field '%s' because the component type '%s' cannot be converted to the field type '%s'", ci.getWicketId(), ij.getPropertyName(), clazz.getName(), ij.getType().getName());
				return null;
			}
		} else {
			clazz = ij.getType();
		}

		Factory factory;

		if(ij.getFactoryMethod() != null) {
			factory = new FactoryMethodComponentFactory(ij.getFactoryMethod());
		} else if(ci.isWicketElement("fragment")) {
			if(!FragmentBuilder.class.isAssignableFrom(ij.getType())) {
				error(errors, InjectorError.Type.INJECTION_POINT_IS_NOT_FRAGMENTBUILDER, "the markup defines a wicket:fragment for wicket:id='%s', but the injection point is not of type %s", ci.getWicketId(), FragmentBuilder.class.getSimpleName());
				return null;
			}
			
			Class<?> typeArg = ij.getTypeArguments()[0];
			Class<? extends Fragment> fragmentClass = (Class<? extends Fragment>)typeArg;

			Recipe recipe = acquireRecipeImpl(fragmentClass, ci, errors);
			
			// find constructor of a Fragment subclass, e.g. Fragment(String,String)
			Constructor constructor = findInjectableConstructor(fragmentClass, errors, containerClass, String.class, String.class, MarkupContainer.class);
			if(constructor == null) {
				return null;
			}
			factory = new FragmentBuilderFactory(constructor, ci.getWicketId(), recipe);
		} else {
			// find constructor for ordinary Wicket component, which takes a single String
			// e.g. Component(String)
			Constructor constructor = findInjectableConstructor(clazz, errors, containerClass, String.class);
			if(constructor == null) {
				return null;
			}
			factory = new ConstructorComponentFactory(constructor);
		}
		
		Class<? extends MarkupContainer> itemClass = ij.getItemClass();
		if(REPEATER_CLASS.isAssignableFrom(factory.getType())) {
			Class<? extends AbstractRepeater> repeaterClass = factory.getType().asSubclass(AbstractRepeater.class);
			
			Class<? extends MarkupContainer> defaultItemClass = findDefaultItemClass(repeaterClass);
			if(itemClass == null) {
				itemClass = defaultItemClass;
			}
			
			// we acquire the recipe to ensure that the item has the 
			// required injection points
			Recipe itemRecipe = acquireRecipeImpl(itemClass, ci, errors);
			
			if(!defaultItemClass.isAssignableFrom(itemClass)) {
				error(errors, InjectorError.Type.ITEMCLASS_NOT_ASSIGNABLE_REPEATER_ITEMCLASS, "The objects of item class %s specified on the injection point %s cannot be assigned to the component's default item class %s", itemClass, ij.getPropertyName(), defaultItemClass);
				return null;
			}
			
			// repeaters implementing ListItemConsumer need their item factory
			// set - we handle this by wrapping the factory by another factory
			if(ListItemConsumer.class.isAssignableFrom(repeaterClass)) {
				Constructor itemConstructor = findInjectableConstructor(itemClass, errors, containerClass, int.class, IModel.class);
				if(itemConstructor == null) {
					return null;	// error should have already been handled
				}
				factory = new ItemConsumerFactory(factory, itemConstructor, itemRecipe);
			}
			
		} else if(itemClass != null) {
			error(errors, InjectorError.Type.ITEMCLASS_ON_NON_REPEATER_INJECTION_POINT, "injection point %s specifies an item class, but its type %s is not a subtype of %s", ij.getFactoryMethod(), ij.getClass().getName(), REPEATER_CLASS);
			return null;
		}
		
		return new Injectable(ij, factory);
	}
	
	private Constructor findInjectableConstructor(Class clazz, List<InjectorError> errors, Class containerClass, Class... parameterTypes) {
		if(Modifier.isAbstract(clazz.getModifiers())) {
			// if a class is abstract (which is often the case for Wicket components,
			// as they're intended to be overwritten), we first check if we
			// have an implementation
			Class implementation = findImplementation(clazz);
			if(implementation == null) {
				error(errors, InjectorError.Type.ABSTRACT_CLASS_WITHOUT_IMPLEMENTATION, "The injection target class %s is abstract, and no implementation could be found.", clazz.getName(), containerClass.getName());
				return null;
			}
			clazz = implementation;
		}
		
		Constructor constructor;
		try {
			Class[] actualParameterTypes;
			int offset;
			if(clazz.isMemberClass() && !Modifier.isStatic(clazz.getModifiers())) {
				if(clazz.getEnclosingClass() != containerClass) {
					error(errors, InjectorError.Type.ENCLOSING_CLASS_NOT_CONTAINER_CLASS, "Cannot inject component of nonstatic inner class '%s', because its enclosing class is not host container's class '%s'", clazz.getName(), containerClass.getName());
					return null;
				}
				actualParameterTypes = new Class[parameterTypes.length+1];
				actualParameterTypes[0] = containerClass;
				offset = 1;
			} else {
				actualParameterTypes = new Class[parameterTypes.length];
				offset = 0;
			}
			
			for(int i=0; i<parameterTypes.length; ++i) {
				actualParameterTypes[offset + i] = parameterTypes[i];
			}
			constructor = clazz.getConstructor(actualParameterTypes);
			constructor.setAccessible(true);
			
			return constructor;
		} catch (NoSuchMethodException ex) {
			StringBuilder sb = new StringBuilder();
			for(Class argClass : parameterTypes) {
				if(sb.length() > 0) {
					sb.append(',');
				}
				sb.append(argClass.getName());
			}
			error(errors, InjectorError.Type.COMPONENT_CONSTRUCTOR_NOT_FOUND, "Cannot find constructor %s(%s) for component class '%s'", clazz.getSimpleName(), sb.toString(), clazz.getName());
			return null;
		} catch (SecurityException ex) {
			error(errors, InjectorError.Type.CONSTRUCTOR_INACCESSIBLE, "Component constructor is inaccessible, nested error message is: '%s'", ex.getMessage());
			return null;
		}		
	}
	
	private <T> Class<? extends T> findImplementation(Class<T> clazz) {
		return componentRegistry.findImplementation(clazz);
	}
	
	private Class<? extends MarkupContainer> findDefaultItemClass(Class<? extends AbstractRepeater> repeaterClass) {
		return componentRegistry.findDefaultItemClass(repeaterClass);
	}
	
	public void validate(Class<? extends MarkupContainer> containerClass, HostCompound markupModel) throws InjectorValidationException {
		// for validation, we simple run acquireInjectables, which will
		// do all error validation for us.
		Recipe recipe = acquireRecipe(containerClass, markupModel);
	}


	public void injectComponents(MarkupContainer container, HostCompound markupModel) throws InjectorValidationException {
		// NOTE: we could cache recipe for each combination of class and markupModel
		Recipe recipe;
		if(markupModel != null) {
			recipe = acquireRecipe(container.getClass(), markupModel);
		} else {
			recipe = getNextComponentRecipe();
			setNextComponentRecipe(null);
		}
		if(recipe != null) {
			injectComponentsImpl(container, recipe);
		}
	}
	private void injectComponentsImpl(MarkupContainer container, Recipe recipe) throws InjectorValidationException {
		
		List<Injectable> injectables = recipe.getInjectables();
		
		// create components and inject them into the target container
		Map<String,Component> instanceMap = new HashMap<String,Component>();
		for(Injectable injectable : injectables) {
			Object instance = injectable.inject(container);
			if(instance instanceof Component) {
				Component injectedComponent = (Component)instance;
				instanceMap.put(injectable.injectionPoint.getPropertyName(), injectedComponent);
			}
		}
		
		// build hierarchy by executing commands
		MarkupContainer parent = container;
		for(HierarchyBuilderCmd cmd : recipe.hierarchyBuilderCommands) {
			
			switch(cmd.type) {
				case SET_PARENT:
					if(cmd.wicketId != null) {
						parent = (MarkupContainer)instanceMap.get(cmd.wicketId);
					} else {
						parent = container;
					}
					break;
					
				case ADD_TO_PARENT:
					Component component = instanceMap.get(cmd.wicketId);
					parent.add(component);
					break;
				
				default:
					throw new UnsupportedOperationException(cmd.type.name());
				
			}
		}
	}
	

	private void buildHierarchyCommands(Class<? extends MarkupContainer> containerClass, Map<String, InjectionPoint> injectionPointMap, List<ComponentInstance> instances, ComponentInstance parent, List<HierarchyBuilderCmd> commands, List<InjectorError> errors, Set<String> ignoredWicketIds) {
		
		HierarchyBuilderCmd setParentCommand = new HierarchyBuilderCmd(HierarchyBuilderCmd.Type.SET_PARENT, parent != null ? parent.getWicketId() : null);
		
		for(ComponentInstance c : instances) {
			if(ignoredWicketIds.contains(c.getWicketId())) {
				continue;
			}
			InjectionPoint ij = injectionPointMap.get(c.getWicketId());
			
			boolean isComponent;
			boolean ignoreChildren;
			if(ij != null) {
				// we ignore an AbstractRepeater's children, because they are
				// generated on the fly by the repeaters populate methods; they
				// also may appear multiple times, making injection into the
				// container pointless for now.
				// Children mapped to a fragment are ignored, as well for the same reason
				isComponent = Component.class.isAssignableFrom(ij.getType());
				ignoreChildren = !isComponent || AbstractRepeater.class.isAssignableFrom(ij.getType());
			} else {
				// error case, but we keep going to collect more potential errors
				ignoreChildren = false;
				isComponent = false;
				error(errors, InjectorError.Type.MISSING_INJECTION_POINT, "No injection point found for wicket:id='%s' in class %s", c.getWicketId(), containerClass.getName());
			}
			
			if(ignoreChildren) {
				addIgnoredChildren(c.getChildren(), injectionPointMap, ignoredWicketIds, errors);
			}
			
			if(isComponent) {
				if(!c.getChildren().isEmpty()) {
					commands.add(new HierarchyBuilderCmd(HierarchyBuilderCmd.Type.SET_PARENT, c.getWicketId()));
					buildHierarchyCommands(containerClass, injectionPointMap, c.getChildren(), c, commands, errors, new HashSet<String>(ignoredWicketIds));
					commands.add(setParentCommand);
				}
			
				commands.add(new HierarchyBuilderCmd(HierarchyBuilderCmd.Type.ADD_TO_PARENT, c.getWicketId()));
			}
		}
	}
	
	private void addIgnoredChildren(List<ComponentInstance> children, Map<String, InjectionPoint> injectionPointMap, Set<String> ignoredWicketIds, List<InjectorError> errors) {
		for(ComponentInstance c : children) {
			String wicketId = c.getWicketId();
			if(injectionPointMap.containsKey(wicketId)) {
				error(errors, InjectorError.Type.INJECTION_POINT_MAPPED_TO_IGNORED_COMPONENT_INSTANCE, "The injection point '%1$s' matches markup with wicket:id='%1$s', but that markup is concealed by a parent component and cannot be referenced.", wicketId);
			}
			ignoredWicketIds.add(wicketId);
		}
	}

	private void error(List<InjectorError> errors, final InjectorError.Type type, final String format, final Object... args) {
		// FIXME: these errors should be collected rather than thrown
		errors.add(new InjectorError() {

			public InjectorError.Type getType() {
				return type;
			}

			public String formatMessage() {
				return String.format(format, (Object[])args);
			}
		});
	}
		
	private void setNextComponentRecipe(Recipe recipe) {
		nextComponentRecipeTL.set(recipe);
	}

	private Recipe getNextComponentRecipe() {
		return nextComponentRecipeTL.get();
	}
}
