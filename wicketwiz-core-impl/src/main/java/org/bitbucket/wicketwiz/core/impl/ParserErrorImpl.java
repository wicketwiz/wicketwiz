/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.wicketwiz.core.impl;

import org.bitbucket.wicketwiz.core.parser.Location;
import org.bitbucket.wicketwiz.core.parser.ParserError;

/**
 *
 * @author count
 */
class ParserErrorImpl implements ParserError{
	private Type type;
	private Location location;
	private String format;
	private Object[] args;

	public ParserErrorImpl() {
	}

	ParserErrorImpl(Type type, Location l, String format, Object... args) {
		this.type = type;
		this.location = l;
		this.format = format;
		this.args = args;
	}

	public Location getLocation() {
		return location;
	}

	public String formatMessage() {
		return String.format(format, args);
	}

	public Type getType() {
		return type;
	}
}
