/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.wicketwiz.core.impl;

import org.bitbucket.wicketwiz.core.model.ClassName;
import org.bitbucket.wicketwiz.core.model.HostCompound;

/**
 *
 * @author count
 */
public class HostCompoundImpl extends AbstractCompoundImpl implements HostCompound{
	
	private ClassName baseClassName;
	private ClassName targetClassName;
	
	@Override
	public ClassName getBaseClassName() {
		return baseClassName;
	}

	@Override
	public ClassName getTargetClassName() {
		return targetClassName;
	}

	void setBaseClassName(ClassName baseClassName) {
		this.baseClassName = baseClassName;
	}

	void setTargetClassName(ClassName targetClassName) {
		this.targetClassName = targetClassName;
	}
}
