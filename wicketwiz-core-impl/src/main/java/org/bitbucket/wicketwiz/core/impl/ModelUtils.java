/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.wicketwiz.core.impl;

import java.util.Map;
import org.bitbucket.wicketwiz.core.model.ComponentInstance;
import org.bitbucket.wicketwiz.core.model.Compound;

/**
 *
 * @author count
 */
public class ModelUtils {

	public static void findComponentInstances(Compound compound, Map<String, ComponentInstance> instances) {
		for (ComponentInstance ci : compound.getChildren()) {
			instances.put(ci.getWicketId(), ci);
			findComponentInstances(ci, instances);
		}
	}
	
}
