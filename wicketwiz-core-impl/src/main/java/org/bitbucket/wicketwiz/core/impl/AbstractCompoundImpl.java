/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.wicketwiz.core.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import org.bitbucket.wicketwiz.core.model.ComponentInstance;
import org.bitbucket.wicketwiz.core.model.Compound;

/**
 *
 * @author upachler
 */
abstract class AbstractCompoundImpl implements Compound {
	private List<ComponentInstance> children = new ArrayList<ComponentInstance>();

	@Override
	public List<ComponentInstance> getChildren() {
		return Collections.unmodifiableList(children);
	}

	void addChild(ComponentInstanceImpl child) {
		children.add(child);
	}

	
}
