/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.wicketwiz.core.impl;

import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Deque;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.bitbucket.wicketwiz.core.model.ClassName;
import org.bitbucket.wicketwiz.core.model.HostCompound;
import org.bitbucket.wicketwiz.core.parser.Location;
import org.bitbucket.wicketwiz.core.parser.PageParser;
import org.bitbucket.wicketwiz.core.parser.PageParserException;
import org.bitbucket.wicketwiz.core.parser.ParserError;
import static org.bitbucket.wicketwiz.core.parser.ParserError.Type.*;
import org.xml.sax.Attributes;
import org.xml.sax.ContentHandler;
import org.xml.sax.InputSource;
import org.xml.sax.Locator;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

/**
 *
 * @author count
 */
public class PageParserImpl implements PageParser, ContentHandler {

	private static final String HTML_NS = "﻿http://www.w3.org/1999/xhtml";

	private static final String WICKET_NS = "http://wicket.apache.org";
	private static final String WICKET_ID_ATT_NAME = "id";

	private static final String WIZ_NS = "urn:org.bitbucket.wicketwiz.markup.ns";
	private static final String WIZ_BASECLASS_ATT_NAME = "baseclass";
	private static final String WIZ_COMPONENT_ATT_NAME = "component";
	private static final String WIZ_IGNORE_ATT_NAME = "ignore";

	private static final Pattern PATH_EXTENSION_SPLIT_PATTERN = Pattern.compile("((?:.*/)?[^\\./]+)(\\.[^\\./]*)?");
	
	private final URI baseUri;

	private static class Context{
		public final AbstractCompoundImpl parent;
		public final boolean ignore;

		public Context(AbstractCompoundImpl parent, boolean ignore) {
			this.parent = parent;
			this.ignore = ignore;
		}
		
	}
	// parser state
	private HostCompoundImpl hostCompound;
	private ClassName targetClassName;
	private Locator locator;
	private final Deque<Context> contextStack = new ArrayDeque<Context>();
	private String wizPrefix;
	private Object wicketPrefix;
	private Locator firstWicketIdLocation;
	private final List<ParserError> errors = new ArrayList<ParserError>();

	private void clearState() {
		hostCompound = null;
		targetClassName = null;
		locator = null;
		contextStack.clear();
		contextStack.add(new Context(null, false));
		wizPrefix = null;
		wicketPrefix = null;
		firstWicketIdLocation = null;
		errors.clear();
		locator = null;
	}
	public PageParserImpl(URI baseUri) {
		this.baseUri = baseUri;
	}
	
	
	public HostCompound parsePage(URI sourcePath) throws IOException, PageParserException {
		return parsePage(sourcePath, null);
	}

	public HostCompound parsePage(URI sourcePath, InputStream inputStream) throws IOException, PageParserException {
		
		clearState();
		
		InputStream ownStream = null;
		try {
			sourcePath = URIUtils.enforceRelative(sourcePath, baseUri);
			
			if(inputStream == null) {
				ownStream = resolveUri(baseUri, sourcePath).toURL().openStream();
				inputStream = ownStream;
			}

			Matcher m = PATH_EXTENSION_SPLIT_PATTERN.matcher(sourcePath.getPath());
			if(!m.matches()) {
				throw new IllegalArgumentException("illegal source path specified; make sure that the file name part only contains one dot");
			}
			String pathWithoutExtension = m.group(1);

			targetClassName = ClassName.valueOf(pathWithoutExtension.replace('/', '.'));

			XMLReader r = XMLReaderFactory.createXMLReader();
			
			try {
				r.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
				r.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
			} catch(SAXException sx) {
				// this does not appear to be xerces -> ignore
			}
			
			r.setContentHandler(this);
			InputSource inputSource = new InputSource(inputStream);
			inputSource.setSystemId(sourcePath.toString());
			r.parse(inputSource);

		} catch (SAXException ex) {
			processingError(MALFORMED_XML, "Could not parse input file: %s", ex.getMessage());
		} finally {
			if(ownStream != null) {
				ownStream.close();
			}
		}
		
		if(!errors.isEmpty()) {
			throw new PageParserException(errors.toArray(new ParserError[errors.size()]));
		}
		
		return hostCompound;
	}

	public void setDocumentLocator(Locator locator) {
		this.locator = locator;
	}

	public void startDocument() throws SAXException {

	}

	public void endDocument() throws SAXException {
	}

	public void startPrefixMapping(String prefix, String uri) throws SAXException {
		if(uri.equals(WIZ_NS)) {
			wizPrefix = prefix;
			if(firstWicketIdLocation != null) {
				processingError(WIZ_NS_AFTER_FIRST_WICKETID, "The first WicketID was already encountered at line %d; the WicketWiz namespace must be defined before that", firstWicketIdLocation.getLineNumber());
			}
			if(hostCompound != null) {
				// if hostCompound already exists, this means the user defined
				// xmlns:wiz=... previously somewhere
				processingError(WIZ_NS_OCCURS_MULTIPLE_TIMES, "xmlns for WicketWiz namespace enountered a second time in this HTML file. It may only be used once.");
			}
			hostCompound = new HostCompoundImpl();
			hostCompound.setTargetClassName(targetClassName);
		} else if (uri.equals(WICKET_NS)) {
			wicketPrefix = prefix;
		}
		
	}

	public void endPrefixMapping(String prefix) throws SAXException {
		if(wizPrefix != null && prefix.equals(wizPrefix)){
			wizPrefix =  null;
		} else if(wicketPrefix != null && prefix.equals(wicketPrefix)) {
			wicketPrefix = null;
		}
	}

	public void startElement(String uri, String localName, String qName, Attributes atts) throws SAXException {
		String baseClassName = atts.getValue(WIZ_NS, WIZ_BASECLASS_ATT_NAME);
		String wicketId = atts.getValue(WICKET_NS, WICKET_ID_ATT_NAME);
		String componentClassName = atts.getValue(WIZ_NS, WIZ_COMPONENT_ATT_NAME);
		String ignoreString = atts.getValue(WIZ_NS, WIZ_IGNORE_ATT_NAME);
		boolean ignore = false;
		
		if(ignoreString != null) {
			if(!ignoreString.equals("true") && !ignoreString.equals("false")) {
				processingError(WIZ_IGNORE_ATTRIBUTE_MALFORMED, "wiz:ignore attrbute may only hold the values 'true' or 'false'");
			} else {
				ignore = Boolean.parseBoolean(ignoreString);
			}
		}
		
		if (baseClassName != null && (wicketId != null || componentClassName != null)) {
			processingError(BASECLASS_ATTRIBUTE_CONFLICT, "%s and %s wiz:baseClass or wiz:component may not appear on the same HTML element", wicketIdAtt(), baseClassAtt());
			baseClassName = null;	// we operate without baseclass in the error case.
		}

		if (baseClassName != null) {
			if (hostCompound.getBaseClassName() != null) {
				processingError(BASECLASS_ATTRIBUTE_ALREADY_DEFINED, "baseclass was already defined as '%s' previously in this file, it may only be defined once", hostCompound.getBaseClassName().getName());
			} else {
				hostCompound.setBaseClassName(parseClassName(baseClassName));
			}
		}
		
		Context context = contextStack.peek();
		
		if(!context.ignore && ignore) {
			context = new Context(context.parent, ignore);
		}
		
		if(componentClassName != null && wicketId == null) {
			processingError(COMPONENTCLASS_ATTRIBUTE_WITHOUT_WICKETID, "%1$s:%2$s cannot appear without %3$:%4", wizPrefix, WIZ_COMPONENT_ATT_NAME, wicketPrefix, WICKET_ID_ATT_NAME);
		}
		
		if(wicketId != null && firstWicketIdLocation != null) {
			firstWicketIdLocation = cloneLocator();
		}
		
		if (!context.ignore && wicketId != null && hostCompound != null) {
			ComponentInstanceImpl.ElementType elementType;
			if(uri.equals(WICKET_NS)) {
				elementType = ComponentInstanceImpl.ElementType.WICKET;
			} else {
				elementType = ComponentInstanceImpl.ElementType.XHTML;
			}
			ComponentInstanceImpl ci = new ComponentInstanceImpl(wicketId, elementType, localName, getHtmlAttMap(atts));
			if(componentClassName != null) {
				ci.setComponentClassName(parseClassName(componentClassName));
			}
			
			if (context.parent==null) {
				hostCompound.addChild(ci);
			} else {
				context.parent.addChild(ci);
			}
			
			context = new Context(ci, context.ignore);
		}
		
		contextStack.push(context);
	}

	public void endElement(String uri, String localName, String qName) throws SAXException {
		contextStack.pop();
	}

	public void characters(char[] ch, int start, int length) throws SAXException {
	}

	public void ignorableWhitespace(char[] ch, int start, int length) throws SAXException {
	}

	public void processingInstruction(String target, String data) throws SAXException {
	}

	public void skippedEntity(String name) throws SAXException {
	}

	private ClassName parseClassName(String classNameString) throws SAXException {
		try {
			return ClassName.valueOf(classNameString);
		} catch (IllegalArgumentException iax) {
			processingError(CLASSNAME_MALFORMED, "'%s' is not a recognized Java class name", classNameString);
			return null;
		}
	}

	private String wicketIdAtt() {
		// FIXME: the prefix should be determined from the current mapping
		return "wicket:id";
	}

	private String baseClassAtt() {
		// FIXME: the prefix should be determined from the current mapping
		return "wiz:baseclass";
	}

	private String componentAtt() {
		// FIXME: the prefix should be determined from the current mapping
		return "wiz:component";
	}

	public URI getBaseUri() {
		return baseUri;
	}

	private Locator cloneLocator() {
		final String publicId = locator.getPublicId();
		final String systemId = locator.getSystemId();
		final int lineNumber = locator.getLineNumber();
		final int columnNumber = locator.getColumnNumber();
		return new Locator() {

			public String getPublicId() {
				return publicId;
			}

			public String getSystemId() {
				return systemId;
			}

			public int getLineNumber() {
				return lineNumber;
			}

			public int getColumnNumber() {
				return columnNumber;
			}
		};
	}

	private Map<String, String> getHtmlAttMap(Attributes atts) {
		if(atts.getLength()==0) {
			return Collections.emptyMap();
		}
		Map<String,String> result = new HashMap<String,String>(atts.getLength());
		for(int i=atts.getLength()-1; i>=0; --i) {
			if(!HTML_NS.equals(atts.getURI(i))){
				continue;
			}
			result.put(atts.getLocalName(i), atts.getValue(i));
		}
		return result;
	}

	private void processingError(ParserError.Type type, String format, Object... args) {
		Location l = new LocationImpl(cloneLocator());
		errors.add(new ParserErrorImpl(type, l, format, args));
	}

	private URI resolveUri(URI baseUri, URI sourcePath) {
		String baseUriString = baseUri.toString();
		if(baseUri.isOpaque()) {
			// handle jar:.. URIs
			if(!sourcePath.isAbsolute()) {
				return URI.create(baseUriString + sourcePath.getPath());
			} else {
				return sourcePath;
			}
		} else {
			return baseUri.resolve(sourcePath);
		}
	}
}
