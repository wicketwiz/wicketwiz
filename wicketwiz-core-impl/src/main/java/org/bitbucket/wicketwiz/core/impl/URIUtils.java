/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.wicketwiz.core.impl;

import java.net.URI;

/**
 *
 * @author count
 */
class URIUtils {

	static URI enforceRelative(URI sourcePath, URI baseUri) {
		if(sourcePath.isOpaque() && baseUri.isOpaque()) {
			// this is to catch jar:file... URIs
			String sourcePathString = sourcePath.toString();
			String baseUriString = baseUri.toString();
			if(sourcePathString.startsWith(baseUriString)) {
				return URI.create(sourcePathString.substring(baseUriString.length()));
			}
		}
		if(sourcePath.isAbsolute()) {
			sourcePath = baseUri.relativize(sourcePath);
			if(sourcePath.isAbsolute()) {
				throw new IllegalArgumentException(String.format("The given URI '%s' is not relative and cannot be made relative against the base URI '%s'", sourcePath, baseUri));
			}
		}
		return sourcePath;
	}
	
}
