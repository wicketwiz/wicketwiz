/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.wicketwiz.core.impl;

import java.net.URI;
import org.bitbucket.wicketwiz.core.WicketWizFactory;
import org.bitbucket.wicketwiz.core.codegen.CodeGenerator;
import org.bitbucket.wicketwiz.core.introspector.ComponentInjector;
import org.bitbucket.wicketwiz.core.parser.PageParser;

/**
 *
 * @author count
 */
public class WicketWizFactoryImpl extends WicketWizFactory{

	@Override
	public CodeGenerator createCodeGenerator(URI outputBaseUri) {
		return new CodeGeneratorImpl(outputBaseUri, "", "Stub");
	}

	@Override
	public PageParser createPageParser(URI inputBaseURI) {
		return new PageParserImpl(inputBaseURI);
	}

	@Override
	public ComponentInjector createInjector() {
		return new ComponentInjectorImpl();
	}
	
}
