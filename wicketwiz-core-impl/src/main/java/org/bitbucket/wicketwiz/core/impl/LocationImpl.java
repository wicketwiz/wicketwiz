/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.wicketwiz.core.impl;

import java.net.URI;
import org.bitbucket.wicketwiz.core.parser.Location;
import org.xml.sax.Locator;

/**
 *
 * @author count
 */
class LocationImpl implements Location {
	private final Locator locator;
	
	public LocationImpl(Locator loc) {
		this.locator = loc;
	}

	public URI getUri() {
		
		return locator.getSystemId()==null ? null : URI.create(locator.getSystemId());
	}

	public int getLine() {
		return locator.getLineNumber();
	}

	public int getColumn() {
		return locator.getColumnNumber();
	}
	
}
