/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.wicketwiz.core.impl;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URI;
import java.nio.charset.Charset;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import org.bitbucket.wicketwiz.core.annotation.Wiz;
import org.bitbucket.wicketwiz.core.codegen.CodeGenerator;
import org.bitbucket.wicketwiz.core.model.ClassName;
import org.bitbucket.wicketwiz.core.model.ComponentInstance;
import org.bitbucket.wicketwiz.core.model.Compound;
import org.bitbucket.wicketwiz.core.model.HostCompound;

/**
 *
 * @author upachler
 */
public class CodeGeneratorImpl implements CodeGenerator {

	private static final Charset UTF_8_CHARSET = Charset.forName("UTF-8");

	private static final String DEFAULT_CONSTRUCTOR_FORMAT = "%s(){\n";
	private static final ClassName DEFAULT_BASE_CLASS_NAME = ClassName.valueOf("org.apache.wicket.markup.html.WebPage");
	private static final ClassName DEFAULT_COMPONENT_CLASSNAME = ClassName.valueOf("org.apache.wicket.markup.html.WebMarkupContainer");
	
	private static final Map<ClassName, CustomConstructor> constructorFormats = new HashMap<ClassName, CustomConstructor>();
	private final String baseIndent = "    ";
	private String indent = "";

	private final URI baseUri;
	private final String namePrefix;
	private final String nameSuffix;
	
	{
		constructorFormats.put(
			new ClassName("org.apache.wicket.markup.html", "WebPage"), 
			new CustomConstructor("%s(PageParameters parameters) { \n%ssuper(parameters);\n", new ClassName("org.apache.wicket.request.mapper.parameter", "PageParameters"))
		);
	}

	public CodeGeneratorImpl(URI baseUri, String namePrefix, String nameSuffix) {
		this.baseUri = baseUri;
		this.namePrefix = namePrefix;
		this.nameSuffix = nameSuffix;
	}

	
	@Override
	public void generate(HostCompound hostCompound) throws IOException {
		
		ClassName effectiveClassName = getEffectiveClassName(hostCompound);
		StringBuilder sb = new StringBuilder(effectiveClassName.getPackageName().replace('.', '/'));
		if(sb.length() > 0) {
			sb.append('/');
		}
		sb.append(effectiveClassName.getSimpleName());
		sb.append(".java");
		
		URI targetURI = URI.create(sb.toString());
		
		OutputStream os = null;
		try {
			URI outputUri = baseUri.resolve(targetURI);
			if(outputUri.getScheme().equals("file")) {
				File outputFile = new File(outputUri);
				outputFile.getParentFile().mkdirs();
				os = new FileOutputStream(outputFile);
			} else {
				os = outputUri.toURL().openConnection().getOutputStream();
			}
			generate(hostCompound, os);
		} finally {
			if(os != null) {
				os.close();
			}
		}
	}

	public void generate(HostCompound hostCompound, OutputStream os) throws IOException {
		
		Writer w = new OutputStreamWriter(os, UTF_8_CHARSET);
			
		generateImpl(hostCompound, w);
		
		w.flush();
	}

	private void generateImpl(HostCompound hostCompound, Writer w) throws IOException {
		
		ClassName effectiveClassName = getEffectiveClassName(hostCompound);
		
		ClassName baseClassName = hostCompound.getBaseClassName();
		if(baseClassName == null) {
			baseClassName = DEFAULT_BASE_CLASS_NAME;
		}
		
		CustomConstructor customConstructor = constructorFormats.get(baseClassName);
		
		// generate package statement
		w.write(indent());
		w.write("package ");
		w.write(effectiveClassName.getPackageName());
		w.write(";\n");
		w.write("\n");

		// generate imports
		TreeSet<ClassName> importClasses = new TreeSet<ClassName>();
		importClasses.add(ClassName.valueOf(Wiz.class.getName()));
		importClasses.add(baseClassName);
		findClassesToImport(hostCompound, importClasses);
		if(customConstructor != null) {
			importClasses.addAll(Arrays.asList(customConstructor.importedClasses));
		}
		for (ClassName className : importClasses) {
			w.write(indent());
			w.write("import ");
			w.write(className.getName());
			w.write(";\n");
		}
		w.write("\n");

		// write class declaration
		w.write(indent());
		w.write("public abstract class ");
		w.write(effectiveClassName.getSimpleName());
		w.write(" extends ");
		w.write(baseClassName.getSimpleName());
		w.write(" {\n");

		indent(+1);
		
		// gather used components
		Map<String, ComponentInstance> componentInstances = new TreeMap<String, ComponentInstance>();
		ModelUtils.findComponentInstances(hostCompound, componentInstances);

		// write members
		for (ComponentInstance ci : componentInstances.values()) {
			w.write(indent());
			w.write("@" + Wiz.class.getSimpleName());
			w.write("\n");
			w.write(indent());
			w.write(getComponentClassName(ci).getSimpleName());
			w.write(' ');
			w.write(ci.getWicketId());
			w.write(";\n");
		}
		w.write("\n");

		// write constructor declaration start
		String constructorFormat;
		if (customConstructor == null) {
			constructorFormat = DEFAULT_CONSTRUCTOR_FORMAT;
		} else {
			constructorFormat = customConstructor.constructorFormat;
			
		}
		
		w.write(indent());
		w.write(String.format(constructorFormat, effectiveClassName.getSimpleName(), indent(+1)));

		// write Wicket component tree construction code
//		writeComponentTreeStatements(hostCompound, w);

		// write constructor end
		w.write(indent(-1));
		w.write("}\n");

		// write end of class
		w.write(indent(-1));
		w.write("}\n");
	}



	private void findClassesToImport(Compound compound, Set<ClassName> classes) {
		for (ComponentInstance ci : compound.getChildren()) {
			classes.add(getComponentClassName(ci));
			findClassesToImport(ci, classes);
		}
	}

	private void writeComponentTreeStatements(Compound compound, Writer w) throws IOException {
		for (ComponentInstance ci : compound.getChildren()) {
			ci.getWicketId();
			w.write(indent);
			ComponentInstance parent = ci.getParentInstance();
			if (parent != null) {
				w.write(parent.getWicketId());
				w.write('.');
			}
			w.write("add(");
			w.write(ci.getWicketId());
			w.write(");\n");

			writeComponentTreeStatements(ci, w);
		}
	}

	private String indent() {
		return indent;
	}
	
	private String indent(int n) {
		if(n>0) {
			StringBuilder sb = new StringBuilder(indent);
			for(int i=0; i<n; ++i) {
				sb.append(baseIndent);
			}
			indent = sb.toString();
		} else {
			indent = indent.substring(0, indent.length() + n*baseIndent.length());
		}
		
		return indent;
	}

	private ClassName getEffectiveClassName(HostCompound hostCompound) {
		ClassName targetClassName = hostCompound.getTargetClassName();
		return new ClassName(targetClassName.getPackageName(), namePrefix + targetClassName.getSimpleName() + nameSuffix);
	}

	private ClassName getComponentClassName(ComponentInstance ci) {
		ClassName cn = ci.getComponentClassName();
		if(cn == null) {
			Map<String,String> elementAttributes = ci.getHtmlElementAttributes();
			if(ci.isHtmlElement("input")) {
				if("button".equals(elementAttributes.get("type"))) {
					cn = ClassName.valueOf("org.apache.wicket.markup.html.form.Button");
				} else {
					cn = ClassName.valueOf("org.apache.wicket.markup.html.form.TextField");
				}
			} else if(ci.isHtmlElement("form")) {
				cn = ClassName.valueOf("org.apache.wicket.markup.html.form.Form");
			} else {
				cn = DEFAULT_COMPONENT_CLASSNAME;
			}
		}
		return cn;
	}

	private static class CustomConstructor {
		public final String constructorFormat;
		public final ClassName[] importedClasses;


		private CustomConstructor(String constructorFormat, ClassName... importedClasses) {
			this.constructorFormat = constructorFormat;
			this.importedClasses = importedClasses;
		}
	}
}
