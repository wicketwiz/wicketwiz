/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.wicketwiz.core.impl;

import java.lang.reflect.Method;
import org.apache.wicket.Component;
import org.apache.wicket.MarkupContainer;

/**
 * An injection point represents a field or method into which Wicket components
 * can be injected.
 */
public interface InjectionPoint {
	
	
	/**
	 * 'property' name of the injection point. For fields, this is the field name,
	 * for methods, this is the method name without the 'set' prefix (the remaining
	 * first letter is lowercased).
	 * @return the property name corresponding to this injection point
	 */
	String getPropertyName();
	
	void inject(MarkupContainer container, Object component);
	
	/**
	 * @return	The factory method to use to instantiate the component, if 
	 * declared; null otherwise.
	 */
	Method getFactoryMethod();
	
	/**
	 * @return the type of the property (field type or setter method argument type)
	 * that will receive the injected component.
	 */
	Class<?> getType();
	
	Class<?>[] getTypeArguments();

	public Class<? extends MarkupContainer> getItemClass();
}
