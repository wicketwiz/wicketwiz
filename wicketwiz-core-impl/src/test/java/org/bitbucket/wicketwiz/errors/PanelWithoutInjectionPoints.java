/*
 * Copyright 2015 count.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.bitbucket.wicketwiz.errors;

import org.apache.wicket.markup.html.panel.Panel;

/**
 *
 * @author count
 */
public class PanelWithoutInjectionPoints extends Panel {

	public PanelWithoutInjectionPoints(String id) {
		super(id);
	}
	
	// WicketWiz is supposed to report an error here, because the corresponding
	// XHTML file contains injectable components
}
