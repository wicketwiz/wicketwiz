package org.bitbucket.wicketwiz.errors;


import java.io.IOException;
import java.net.URISyntaxException;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import org.junit.Test;
import org.bitbucket.wicketwiz.core.WicketWizFactory;
import org.bitbucket.wicketwiz.core.introspector.ComponentInjector;
import org.bitbucket.wicketwiz.core.introspector.InjectorError;
import org.bitbucket.wicketwiz.core.introspector.InjectorValidationException;
import org.bitbucket.wicketwiz.core.model.HostCompound;
import org.bitbucket.wicketwiz.core.parser.PageParser;
import org.bitbucket.wicketwiz.core.parser.PageParserException;
import org.bitbucket.wicketwiz.test.TestBase;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author count
 */
public class TestErrors extends TestBase {
	@Test
	public void testPanelWithoutInjectionPoints() throws PageParserException, IOException, URISyntaxException {
		WicketWizFactory factory = WicketWizFactory.newInstance();
		PageParser parser = factory.createPageParser(testBaseUri());
		
		HostCompound page = parser.parsePage(testClassResourceUri("PanelWithoutInjectionPoints.html"));
		
		ComponentInjector injector = factory.createInjector();
		
		try {
			injector.validate(PanelWithoutInjectionPoints.class, page);
		} catch (InjectorValidationException ex) {
			
			assertEquals(InjectorError.Type.MISSING_INJECTION_POINT, ex.getErrors()[0].getType()); 
			return;
		}
		
		// no exception was thrown, but we're expecting one.
		fail();
	}
}
