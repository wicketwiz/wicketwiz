/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.wicketwiz.test;

import java.net.URI;
import java.net.URISyntaxException;

/**
 *
 * @author count
 */
public class TestBase {

	protected URI testBaseUri() throws URISyntaxException {
		String relativePortion = "root-marker.txt";
		URI uri = getClass().getResource("/" + relativePortion).toURI();
		URI baseUri = uri.resolve(".");
		return baseUri;
	}

	protected URI testClassResourceUri(String s) {
		return URI.create(getClass().getPackage().getName().replace('.', '/') + "/" + s);
	}
	
}
