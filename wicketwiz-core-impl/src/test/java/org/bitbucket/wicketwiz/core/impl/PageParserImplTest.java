/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.wicketwiz.core.impl;

import org.bitbucket.wicketwiz.core.impl.PageParserImpl;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import static java.util.Collections.emptyMap;
import org.junit.Test;
import static org.junit.Assert.*;
import org.bitbucket.wicketwiz.core.model.ClassName;
import org.bitbucket.wicketwiz.core.model.ComponentInstance;
import org.bitbucket.wicketwiz.core.model.HostCompound;
import org.bitbucket.wicketwiz.core.parser.PageParserException;
import org.bitbucket.wicketwiz.test.TestBase;

/**
 *
 * @author count
 */
public class PageParserImplTest extends TestBase {
	
	public PageParserImplTest() {
	}

	/**
	 * Test of parsePage method, of class PageParserImpl.
	 */
	@Test
	public void testParsePage() throws Exception {
		System.out.println("parsePage");

		PageParserImpl instance = new PageParserImpl(testBaseUri());
		
		URI relativeURI = testClassResourceUri("HomePage.html");
		HostCompound result = instance.parsePage(relativeURI);
		
		assertEquals(ClassName.valueOf("org.apache.wicket.markup.html.WebPage"), result.getBaseClassName());
		assertEquals(ClassName.valueOf("org.bitbucket.wicketwiz.core.impl.HomePage"), result.getTargetClassName());
		
		ComponentInstance child1 = result.getChildren().get(0);
		assertEquals(ClassName.valueOf("org.apache.wicket.markup.html.basic.Label"), child1.getComponentClassName());
		assertTrue(child1.getParentInstance() == null);
		assertEquals("version", child1.getWicketId());
	}
	
	@Test
	public void testParseAPanel() throws Exception {
		PageParserImpl instance = new PageParserImpl(testBaseUri());
		
		URI relativeURI = testClassResourceUri("APanel.html");
		HostCompound result = instance.parsePage(relativeURI);
		
		assertEquals(ClassName.valueOf("org.apache.wicket.markup.html.panel.Panel"), result.getBaseClassName());
		
	}
	
	@Test
	public void testParseNoWiz() throws URISyntaxException, IOException, PageParserException {
		PageParserImpl instance = new PageParserImpl(testBaseUri());
		HostCompound result = instance.parsePage(testClassResourceUri("NoWiz.html"));
		
		assertNull(result);
	}
	
	@Test
	public void testParseOnlzWiyNS() throws URISyntaxException, IOException, PageParserException {
		PageParserImpl instance = new PageParserImpl(testBaseUri());
		HostCompound result = instance.parsePage(testClassResourceUri("OnlyWizNS.html"));
		
		assertNotNull(result);
		
		assertEquals(1, result.getChildren().size());
	}
	
	@Test
	public void testParseComplexHierarchy() throws URISyntaxException, IOException, PageParserException {
		PageParserImpl instance = new PageParserImpl(testBaseUri());
		HostCompound result = instance.parsePage(testClassResourceUri("ComplexHierarchy.html"));
		
		assertNotNull(result);
		
		assertEquals(2, result.getChildren().size());
		
		ComponentInstance commentFormCI = result.getChildren().get(0);
		assertTrue(commentFormCI.isHtmlElement("form"));
		assertEquals("commentForm", commentFormCI.getWicketId());
		assertEquals(emptyMap(), commentFormCI.getHtmlElementAttributes());
		assertEquals(2, commentFormCI.getChildren().size());
		
		ComponentInstance commentsCI = result.getChildren().get(1);
		assertEquals("comments", commentsCI.getWicketId());
		assertTrue(commentsCI.isHtmlElement("span"));
		assertEquals(emptyMap(), commentsCI.getHtmlElementAttributes());
		assertEquals(2, commentsCI.getChildren().size());
		
		ComponentInstance commentTextCI = commentFormCI.getChildren().get(0);
		assertEquals("commentText", commentTextCI.getWicketId());
		assertTrue(commentTextCI.isHtmlElement("textarea"));
		assertEquals(emptyMap(), commentTextCI.getHtmlElementAttributes());
		assertEquals(0, commentTextCI.getChildren().size());
	}
	
	@Test(expected = FileNotFoundException.class)
	public void testMissingFile() throws URISyntaxException, IOException, PageParserException {
		PageParserImpl instance = new PageParserImpl(testBaseUri());
		instance.parsePage(URI.create("nonexisting/file.html"));
	}
	
}
