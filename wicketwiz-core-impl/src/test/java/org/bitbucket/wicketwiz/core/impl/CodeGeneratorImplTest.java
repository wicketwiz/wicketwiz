/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.bitbucket.wicketwiz.core.impl;

import java.io.File;
import static org.bitbucket.wicketwiz.core.impl.ComponentInstanceImpl.ElementType.XHTML;
import org.junit.Test;
import org.bitbucket.wicketwiz.core.model.ClassName;

/**
 *
 * @author upachler
 */
public class CodeGeneratorImplTest {
	
	public CodeGeneratorImplTest() {
	}

	
	/**
	 * Test of generate method, of class CodeGeneratorImpl.
	 */
	@Test
	public void testGenerate() throws Exception {
		
		System.out.println("generate");
		
		ComponentInstanceImpl labelComponent = new ComponentInstanceImpl("version", XHTML, "div");
		labelComponent.setComponentClassName(new ClassName("org.apache.wicket.markup.html.basic", "Label"));
		
		HostCompoundImpl hostCompound = new HostCompoundImpl();
		hostCompound.setBaseClassName(new ClassName("org.apache.wicket.markup.html", "WebPage"));
		hostCompound.setTargetClassName(new ClassName("org.bitbucket.wicketwiz.test", "MainPage"));
		hostCompound.addChild(labelComponent);
		
		CodeGeneratorImpl instance = new CodeGeneratorImpl(new File(System.getProperty("java.io.tmpdir")).toURI(), "", "Stub");
		instance.generate(hostCompound, System.out);
		
		// this generates text, so there is not much we can do to check..
	}
	
}
